# Version 2.6.11

* [PRESTA16-243] Facelift purchase-button-patch PRESTA17-90
* ECom 1.3.51 upgrade.

# Version 2.6.10

* [PRESTA16-80] - When the payment cannot be displayed/fetched on the order view we should display an error instead
* [PRESTA16-124] - Simplify modules/resursbankcheckout/models/Api :: validateCredentials()
* [PRESTA16-177] - Lägg till ett Cronjob som automatiskt makulerar beställningar där betalning uteblivit
* [PRESTA16-184] - Lägg till inställning så man själv kan bestämma hur länge beställningar utan betalning ligger kvar innan dem tas bort (PRESTA-170)
* [PRESTA16-234] - Raise compliance for PHP 8.0, etc
* [PRESTA16-239] - Split statuses for credited and annulled
* [PRESTA16-200] - I nyare versioner av ECom kommer automatisk annullering att fallera
* [PRESTA16-201] - Cronjobb cancelerar 0-orders
* [PRESTA16-233] - Cancellationscript should accept cancellations on "payment not found" in cleanupscript
* [PRESTA16-235] - During failed orders, rebuilding cart fails on reload

# Version 2.6.5 - 2.6.9

## Imported notes

* Also included in version releases that is missing, is the ecomphp-library with PHP 8.0-compliances.
* In 2.6.4, we've splitted the plugin into a recustomized model for PrestaShop 1.7.

## The rest of the release notes

* [PRESTA16-222] - Update presta 1.6 ecomphp to latest (And check the bitwise controls for order statuses)
* [PRESTA16-208] - [PartPayment] Partpayment Implementation
* [PRESTA16-212] - [PartPayment] Layout cleanup for admin
* [PRESTA16-89] - Make sure that the failure URL is loaded if order placement fails after clicking the place order button
* [PRESTA16-138] - Translate everything.
* [PRESTA16-145] - Callbacks no longer work after re-registering them
* [PRESTA16-152] - If there are no registered callbacks the button to register callbacks will fail to display.
* [PRESTA16-182] - Uppgradera till senaste versionen av ECom
* [PRESTA16-192] - Kontrollera användarnamn/lösenord och visa ett felmeddelande i konfigen om dessa inte stämmer
* [PRESTA16-196] - Felsök ärende åt Staffanstorp. När kund går till gateway och sedan avbryter skall dem komma tillbaka till kassan och varukorgen skall återskapas.
* [PRESTA16-204] - [1.7] Sessioner
* [PRESTA16-205] - [PartPayment] Admin-panel configuration (Backend)
* [PRESTA16-206] - [PartPayment] Customer view (Frontend)
* [PRESTA16-209] - [Automatisk annullering] Undvik beställningar där callbacks inkommit
* [PRESTA16-210] - [PartPayment] Part payment in checkout
* [PRESTA16-211] - [PartPayment] getCostOfPurchaseHtml popup.
* [PRESTA16-220] - Översättningar till annuity-delarna
* [PRESTA16-221] - [ User Agent ] Lägg till version av Prestashop för att underlätta felsökning
* [PRESTA16-63] - Ingen orderbekräftelselandningssida efter köp
* [PRESTA16-67] - RCO laddas inte om man avbyter Nets och kommer tillbaka till kassan
* [PRESTA16-103] - Callback funkar inte vid återställa plugin
* [PRESTA16-216] - When activating part payment method first time, period is not saved
* [PRESTA16-217] - Fel belopp skickas till "Read more"
* [PRESTA16-218] - Listan över betalmetoder refreshas inte vid kontobyta test till prod
* [PRESTA16-219] - RESURSBANK is not defined (js)
* [PRESTA16-223] - Things that can break on upgrades
* [PRESTA16-224] - Annuity (and methods) are not cleaned up during uninstall/reset
* [PRESTA16-229] - "Pay from" is active even if it is not properly configured
* [PRESTA16-225] - Compare and (try to) import 1.7 hotfixes.
* [PRESTA16-226] - [Kassa] Testköp som gäst och inloggad skall fortsatt fungera utan exceptions
* [PRESTA16-227] - [Admin] Statusar för varje order skall fortsatt fungera utan exceptions
* [PRESTA16-228] - [Kassa] Specialköp: Customized products (om de finns) skall INTE misslyckas till skillnad från Presta 1.7 - yet they still do. And they don't exist in 1.6 the same way.
* [PRESTA16-213] - [PartPayment] Display payment methods
* [PRESTA16-214] - [PartPayment] Display annuity options in admin differently
* [PRESTA16-215] - thead for payment methods list



# Version 1.0.0

* [PRESTA16-51] - Ställ in Prestashop vid installation utav modul
* [PRESTA16-55] - Ta bort grupperingsinställning i admin under modul
* [PRESTA16-57] - Sätt valt betalsätt på ordern
* [PRESTA16-58] - Set v1.0.0 as release version
* [PRESTA16-59] - CSS for admin lost


# Version 0.9.3

* [PRESTA16-17] - Translations
* [PRESTA16-31] - Set icon to Resurs status
* [PRESTA16-52] - Optimize PRESTA-50 (callbacks) furthermore
* [PRESTA16-32] - Follow URLs in higher security environments (curl setting $followLocationEnforce)
* [PRESTA16-35] - Two "save"-clicks are required for callbacks to be saved on credential changes
* [PRESTA16-38] - Need to save two times when changing environment in admin?
* [PRESTA16-50] - Configuration saves slow - Optimize speed by limiting callback update requests to a dynamic interval (subtask/fix for PRESTA-35)
* [PRESTA16-53] - Test event triggers goes to wrong environment (Hardcoded)


# Version 0.9.2

* [PRESTA16-36] - Remove ecomphp tests in final version
* [PRESTA16-37] - Show registered callbacks view (admin gui)
* [PRESTA16-40] - Replace callback if-conditions with case
* [PRESTA16-41] - Callback URL validation
* [PRESTA16-45] - Cache payment methods on checkout page for invoice fee-handling
* [PRESTA16-47] - No calllback testing when "TEST" does not exist
* [PRESTA16-48] - Visa betalmetod i meddelandet
* [PRESTA16-49] - Blå ruta på order i admin
* [PRESTA16-9] - Support: Discount
* [PRESTA16-13] - Handle callbacks by cartid (until orderids are fixed) - Primary functions
* [PRESTA16-33] - Created default statuses with generated mail-responses to customers
* [PRESTA16-34] - Callback adjustments (for PRESTA-13)
* [PRESTA16-44] - Säkerställ att moms och priser skickas korrekt
* [PRESTA16-46] - [ecommerce] getCallbacksByRest() does not entirely reflect the truth in ecommerce

# Version 0.9.1

* [PRESTA16-18] - Country-selector with guess checkout
* [PRESTA16-24] - Show in gui which version of the plugin the current agent is running.
* [PRESTA16-28] - Add prestashop Order# as metadata at ResursBank
* [PRESTA16-29] - Build Resurs-based order statuses
* [PRESTA16-21] - Live / Prod inställningar
* [PRESTA16-30] - Address update fails

# Version 0.9.0

* [PRESTA16-1] - ResursBank logo i lista av moduler
* [PRESTA16-2] - I konfigurera Resurs Bank Ceckout
* [PRESTA16-3] - Bort med steg 1 i kassan
* [PRESTA16-14] - Register callbacks ECom1.2-compatibility
* [PRESTA16-5] - Ändra Artikelbeskrivning
* [PRESTA16-6] - Dold checkout
* [PRESTA16-7] - Synka adressatinformation
* [PRESTA16-8] - Tillbaka till varukorgen vid "fel"
* [PRESTA16-11] - Rename plugin to lowercase
* [PRESTA16-15] - Duplicate terms checkbox

