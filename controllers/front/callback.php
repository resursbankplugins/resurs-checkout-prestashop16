<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Class ResursBankCheckoutCallbackModuleFrontController
 *
 * Routes incoming callback calls.
 */
class ResursBankCheckoutCallbackModuleFrontController extends ModuleFrontController
{
    /**
     * Use SSL if possible.
     *
     * @var bool
     */
    public $ssl = true;

    /**
     * Route incoming callback.
     */
    public function postProcess()
    {
        /** @var \ResursBankCheckout\Models\Callback $callback */
        $callback = new \ResursBankCheckout\Models\Callback($this->module);

        try {
            // Execute callback handler.
            $callback->execute();

            // Everything went fine.
            header('HTTP/1.1 204 Callback accepted', true, 204);
        } catch (Exception $e) {
            $callback->log($e, $_REQUEST);

            header(
                ('HTTP/1.1 406 Something went wrong. Please consult the ' .
                'client side error log for more information.'),
                true,
                406
            );
        }

        die();
    }
}
