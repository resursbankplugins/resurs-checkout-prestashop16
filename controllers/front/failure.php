<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Class ResursBankCheckoutFailureModuleFrontController
 *
 * Handles failed payments. This will be invoked when an error occurs either during order placement, or at the payment
 * gateway.
 */
class ResursBankCheckoutFailureModuleFrontController extends ModuleFrontController
{
    /**
     * Use SSL if possible.
     *
     * @var bool
     */
    public $ssl = true;

    /**
     * Rebuild the shopping cart and redirect to the checkout page.
     */
    public function postProcess()
    {
        try {
            \ResursBankCheckout\Models\Service\Payment::handleFailedPayment(
                $this->getModule(),
                Tools::getValue('cart_id'),
                Tools::getValue('validation_key')
            );

            // Add error message to checkout bag.
            \ResursBankCheckout\Models\Service\Message\Checkout::add(
                \ResursBankCheckout\Models\Service\Message::TYPE_ERROR,
                $this->getModule()->l('Something went wrong while processing the payment. Please try again. If the error persists please select a different payment method.')
            );
        } catch (Exception $e) {
            // Log error.
            $this->getModule()->log($e, $_REQUEST);
        }

        Tools::redirect($this->module->getBaseUrl() . '?controller=orderopc');
    }

    /**
     * @return \ResursBankCheckout
     */
    protected function getModule()
    {
        return $this->module;
    }
}
