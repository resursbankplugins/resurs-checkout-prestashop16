<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Class ResursBankCheckoutSaveOrderModuleFrontController
 *
 * Create order.
 */
class ResursBankCheckoutSaveOrderModuleFrontController extends ModuleFrontController
{
    /**
     * Use SSL if possible.
     *
     * @var bool
     */
    public $ssl = true;

    /**
     * Create order.
     */
    public function postProcess()
    {
        try {
            // Create customer, address and order.
            \ResursBankCheckout\Models\Service\Cart::place($this->getModule());
        } catch (Exception $e) {
            // Message explaining what went wrong. This is inaccurate though and
            // only required for our development client. A permanent solution
            // will be implemented later.
            \ResursBankCheckout\Models\Service\Message\Checkout::add(
                \ResursBankCheckout\Models\Service\Message\Checkout::TYPE_ERROR,
                $this->getModule()->l('We apologize, your session expired and the page had to be reloaded. Your order has not yet been placed. Please fill out your information below to proceed with placing your order.')
            );

            // Log error message.
            $this->getModule()->log($e, $_REQUEST);

            // Display generic error message.
            $this->getAjax()->addError(
                $this->getModule()->l('Something went wrong while placing the order. Please check your provided information try again.')
            );
        }

        $this->getAjax()->respond();
    }

    /**
     * @return \ResursBankCheckout\Models\Controller\Ajax
     */
    protected function getAjax()
    {
        return $this->getModule()->getAjaxController($this);
    }

    /**
     * @return \ResursBankCheckout
     */
    protected function getModule()
    {
        return $this->module;
    }
}
