<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use \ResursBankCheckout\Models\Service\Customer\Address\Billing;

/**
 * Class ResursBankCheckoutSuccessModuleFrontController
 *
 * Display the success page after completing order placement and payment.
 */
class ResursBankCheckoutSuccessModuleFrontController extends ModuleFrontController
{
    /**
     * Use SSL if possible.
     *
     * @var bool
     */
    public $ssl = true;

    /**
     * Do not display left-hand column when rendering thank you page.
     *
     * @var bool
     */
    public $display_column_left = false;
    
    /**
     * Render order confirmation page.
     */
    public function initContent()
    {
        try {
            parent::initContent();

            $cartId = (int) Tools::getValue('id_cart');

            if ($cartId === 0) {
                throw new Exception('Missing cart id.');
            }

            // Clear cookie data.
            $this->context->cookie->id_cart = null;

            /** @var \Cart $cart */
            $cart = new Cart($cartId);

            /** @var \Order $order */
            $order = \ResursBankCheckout\Models\Service\Order::getByCart($cart);

            // Assign template variables.
            $this->context->smarty->assign(array(
                'order' => $order,
                'currency' => Currency::getCurrencyInstance($order->id_currency),
                'shop_name' => Configuration::get('PS_SHOP_NAME'),
                'base_url' => $this->module->getBaseUrl()
            ));

            // Make sure SSN is applied on invoice address.
            Billing::updateMissingSsn($this->module, $order);

            // Render thank you page.
            $this->setTemplate('success.tpl');
        } catch (Exception $e) {
            // Log error.
            $this->module->log($e, $_REQUEST);

            // Redirect to home page.
            Tools::redirect($this->module->getBaseUrl());
        }
    }
}
