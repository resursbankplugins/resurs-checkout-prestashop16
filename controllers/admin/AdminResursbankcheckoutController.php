<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Class ResursBankCheckoutRegisterCallbacksModuleFrontController
 *
 * Handles callback registration (invoked from the configuration page in the administration panel).
 */
class AdminResursbankcheckoutController extends ModuleAdminController
{

    /**
     * AdminResursbankcheckoutController constructor.
     *
     * @throws PrestaShopException
     */
    public function __construct()
    {
        parent::__construct();
        $this->checkAnnuityMethodStatus();
    }

    /**
     * Check if annuity methods duration is requested enabled or disabled by administrator.
     */
    public function checkAnnuityMethodStatus()
    {
        $annuityMethod = Tools::getValue('method');
        $annuitySet = Tools::getValue('set');

        if (!empty($annuityMethod)) {
            if ($annuitySet === 'enabled') {
                \Configuration::updateValue(
                    'RESURSBANK_PARTPAYMENT_METHOD',
                    $annuityMethod
                );
            } else {
                \Configuration::updateValue(
                    'RESURSBANK_PARTPAYMENT_METHOD',
                    ''
                );
            }

            $module = $this->getModule();
            $redirectUrl = $module->getContext()->link->
                getAdminLink(
                    'AdminModules'
                ) .
                '&configure=' .
                $module->getName()
                . '&tab_module=payments_gateways'
                . '&module_name=' . $module->getName();

            Tools::redirectAdmin($redirectUrl);
        }
    }

    /**
     * Register callback URLs.
     */
    public function ajaxProcessRegisterCallbacks()
    {
        try {
            // Register callbacks.
            $this->getModule()->getApi()->getCallbackHandler()->registerCallbacks();

            // Collect new callbacks from API.
            $this->getAjax()->addResponseData(
                'callbacks',
                $this->getModule()->getApi()->getCallbackHandler()->getSortedCallbacks()
            );
        } catch (Exception $e) {
            $this->getModule()->log($e, $_REQUEST);

            $this->getAjax()->addError(
                $this->getModule()->l('Something went wrong while registering your callbacks. Please try again.')
            );
        }

        $this->getAjax()->respond();
    }

    /**
     * Fetch payment methods from API.
     */
    public function ajaxProcessFetchPaymentMethods()
    {
        $ajax = $this->getAjax();
        try {
            \ResursBankCheckout\Models\Service\Payment::updateMethods($this->getModule());
            // Include annuity factors in the local data.
            $this->getAnnuityFactors();
            $ajax->addResponseData('method', \ResursBankCheckout\Models\Service\Payment::getMethods());
        } catch (Exception $e) {
            $this->getModule()->log($e, $_REQUEST);

            $this->getAjax()->addError(
                $this->getModule()->l('Something went wrong while fetching payment methods. Please try again.')
            );
        }

        $this->getAjax()->respond();
    }

    /**
     * Update database if /För is still annoying the system.
     */
    public function ajaxProcessFixTheMetaLang()
    {
        $ajax = $this->getAjax();
        $langTbl = _DB_PREFIX_ . "meta_lang";
        $sqlQuery = "UPDATE " . _DB_PREFIX_ . "meta_lang SET url_rewrite = 'order' WHERE title = 'Beställning' AND url_rewrite = 'För'";
        $sqlUpdate = Db::getInstance()->executeS($sqlQuery);
        $ajax->addResponseData('reload', $sqlUpdate);
        $ajax->respond();
    }

    /**
     * Update duration data for annuity factor through AJAX.
     */
    public function ajaxProcessUpdateAnnuityDuration()
    {
        $ajax = $this->getAjax();
        $data = isset($_REQUEST['data']) ? $_REQUEST['data'] : '';
        $isSaved = false;
        if ($data > 0) {
            $isSaved = \Configuration::updateValue(
                ResursBankCheckout\Models\Service\Config::SETTING_PARTPAYMENT_DURATION,
                $data
            );
        }

        $ajax->addResponseData('save', $isSaved);
        $this->getAjax()->respond();
    }

    /**
     * Fetch and update annuity factors from API.
     *
     * @throws Exception
     */
    protected function getAnnuityFactors()
    {
        try {
            \ResursBankCheckout\Models\Service\Payment::updateAnnuity($this->getModule());
        } catch (Exception $e) {
            $this->getModule()->log($e, $_REQUEST);

            $this->getAjax()->addError(
                $this->getModule()->l('Something went wrong while fetching annuity factors. Please try again.')
            );
        }
    }

    /**
     * @return \ResursBankCheckout\Models\Controller\Ajax
     */
    protected function getAjax()
    {
        return $this->getModule()->getAjaxController($this);
    }

    /**
     * @return \ResursBankCheckout
     */
    protected function getModule()
    {
        return $this->module;
    }
}
