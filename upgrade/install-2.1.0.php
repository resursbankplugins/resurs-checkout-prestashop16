<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

function upgrade_module_2_1_0($object)
{
    return ConfigurationCore::updateValue(
        \ResursBankCheckout\Models\Service\Config::SETTING_SESSION_LIFETIME,
        86400
    );
}
