<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace ResursBankCheckout\Models;

use \Configuration;
use \ResursBankCheckout;
use \ResursBankCheckout\Models\Service\Config;

/**
 * Class General
 *
 * Common methods used to collecting payment lines from objects, such as the shopping cart or an order. There are
 * individual subclasses for each object type payment lines cna be collected from.
 *
 * @package Resursbankcheckout\Models\Items
 */
abstract class Items
{
    /**
     * @var ResursBankCheckout
     */
    protected $module;

    /**
     * Defines which product property should be used as artNo value in outgoing API calls.
     *
     * @var string
     */
    protected $productSkuProperty;

    /**
     * Cart constructor.
     *
     * @param ResursBankCheckout $module
     */
    public function __construct(ResursBankCheckout $module)
    {
        $this->module = $module;
    }

    /**
     * Retrieve order lines used when communicating the Resurs Bank.
     *
     * @param array $productLines
     * @param array $discountLines
     * @param array $shippingLine
     * @return mixed
     */
    protected function compilePaymentLines(
        array $productLines,
        array $discountLines,
        array $shippingLine
    ) {
        $result = $productLines;

        if (count($discountLines) > 0) {
            $result = array_merge($result, $discountLines);
        }

        if (count($shippingLine) > 0) {
            $result[] = $shippingLine;
        }

        return $result;
    }

    /**
     * Retrieve discount payment line.
     *
     * @param string $code
     * @param string $description
     * @param float $priceExclTax
     * @param float $taxPercent
     * @return array
     */
    protected function compileDiscountLine(
        $code,
        $description,
        $priceExclTax,
        $taxPercent
    ) {
        $result = array();

        if ($priceExclTax > 0) {
            $result = array(
                'artNo' => preg_replace('/[^a-z0-9\_]/isU', '', $code),
                'description' => (string) $description,
                'quantity' => 1,
                'unitMeasure' => 'pcs',
                'unitAmountWithoutVat' => -$priceExclTax,
                'vatPct' => $taxPercent,
                'type' => 'DISCOUNT'
            );
        }

        return $result;
    }


    /**
     * Getting payment line for shipping fee.
     *
     * @param float $priceExclTax
     * @param float $taxPercent
     * @param string $description
     * @return array
     */
    protected function compileShippingLine(
        $priceExclTax,
        $taxPercent,
        $description
    ) {
        $result = array();

        if ($priceExclTax > 0) {
            $result = array(
                'artNo' => 'shipping',
                'description' => $description,
                'quantity' => 1,
                'unitMeasure' => 'pcs',
                'unitAmountWithoutVat' => $priceExclTax,
                'vatPct' => $taxPercent,
                'type' => 'SHIPPING_FEE'
            );
        }

        return $result;
    }

    /**
     * Convert Mage_Sales_Model_Quote_Item to an order line for the API.
     *
     * @param string $sku
     * @param string $name
     * @param float $qty
     * @param string $unit
     * @param float $price
     * @param float $taxPercent
     * @return array
     */
    protected function compileProductLine(
        $sku,
        $name,
        $qty,
        $unit,
        $price,
        $taxPercent
    ) {
        return array(
            'artNo' => $sku,
            'description' => $name,
            'quantity' => $qty,
            'unitMeasure' => $unit,
            'unitAmountWithoutVat' => $price,
            'vatPct' => $taxPercent,
            'type' => 'ORDER_LINE'
        );
    }

    /**
     * Takes prices with both including and excluding tax, and returns the calculated shipping tax.
     *
     * @param float $inclTax
     * @param float $exclTax
     * @return float|int
     */
    protected function calculateShippingTax($inclTax, $exclTax)
    {
        return $this->calculateTax($inclTax, $exclTax);
    }

    /**
     * Takes prices with both including and excluding tax, and returns the calculated discount tax.
     *
     * @param float $inclTax
     * @param float $exclTax
     * @return float|int
     */
    protected function calculateDiscountTax($inclTax, $exclTax)
    {
        return $this->calculateTax($inclTax, $exclTax);
    }

    /**
     * Takes prices with both including and excluding tax, and returns the calculated product tax.
     *
     * @param float $inclTax
     * @param float $exclTax
     * @return float|int
     */
    protected function calculateProductTax($inclTax, $exclTax)
    {
        return $this->calculateTax($inclTax, $exclTax);
    }

    /**
     * Simple tax calculation.
     *
     * @param float $inclTax
     * @param float $exclTax
     * @return float|int
     */
    protected function calculateTax($inclTax, $exclTax)
    {
        $result = 0;

        if ($inclTax > $exclTax) {
            $result = ((($inclTax / $exclTax) - 1) * 100);
        }
        
        if (Config::getRoundTaxPercentage()) {
            $result = round($result);
        }

        return $result;
    }
}
