<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace ResursBankCheckout\Models\Service;

use Configuration;
use Db;
use Exception;
use HelperForm;
use ResursBankCheckout;
use Tools;

/**
 * Class Config
 *
 * Config service. Contains methods and constants related to the configuration page/values.
 *
 * @package ResursBankCheckout\Models\Service
 */
class Config
{
    /**
     * Config page tab.
     */
    const TAB = 'payments_gateways';

    /**
     * Config form submit action.
     */
    const CONFIG_SUBMIT_ACTION = 'submitResursBankCheckoutModule';

    /**
     * This setting defines whether or not the module is active.
     */
    const SETTING_ACTIVE = 'RESURSBANKCHECKOUT_LIVE_MODE';

    /**
     * This setting defines whether or not we round tax percentage values.
     */
    const SETTING_ROUND_TAX_PERCENTAGE = 'RESURSBANKCHECKOUT_ROUND_TAX_PERCENTAGE';

    /**
     * Setting that keeps the username for the production environment.
     */
    const SETTING_PRODUCTION_USERNAME = 'RESURSBANKCHECKOUT_LIVE_ACCOUNT_USERNAME';

    /**
     * Setting that keeps the password for the production environment.
     */
    const SETTING_PRODUCTION_PASSWORD = 'RESURSBANKCHECKOUT_LIVE_ACCOUNT_PASSWORD';

    /**
     * Setting that keeps the username for the test environment.
     */
    const SETTING_TEST_USERNAME = 'RESURSBANKCHECKOUT_TEST_ACCOUNT_USERNAME';

    /**
     * Setting that keeps the password for the test environment.
     */
    const SETTING_TEST_PASSWORD = 'RESURSBANKCHECKOUT_TEST_ACCOUNT_PASSWORD';

    /**
     * Identifier of the setting containing currently selected API environment.
     */
    const SETTING_ENVIRONMENT = 'RESURSBANKCHECKOUT_PRODUCTION_MODE';

    /**
     * Setting that keeps the callback SALT value.
     */
    const SETTING_SALT = 'RESURSBANKCHECKOUT_SALT';

    /**
     * This setting contains the previous values of all settings we override during installation, so we can re-apply
     * them if we uninstall the module.
     */
    const SETTING_OVERRIDE_CONFIG_DEFAULTS = 'RESURSBANKCHECKOUT_CONFIG_OVERRIDE';

    /**
     * This setting is used to enable/disable debug logging.
     */
    const SETTING_DEBUG_ENABLED = 'RESURSBANKCHECKOUT_DEBUG_ENABLED';

    /**
     * This setting is used to specify whether or not we will be storing SSN on the billing address.
     */
    const SETTING_STORE_SSN = 'RESURSBANKCHECKOUT_STORE_SSN';

    /**
     * List of available payment methods.
     */
    const SETTING_PAYMENT_METHODS = 'RESURSBANK_PAYMENT_METHODS';

    /**
     * List of payment methods that support annuity factors.
     */
    const SETTING_ANNUITY_FACTORS = 'RESURSBANK_ANNUITY_FACTORS';

    /**
     * Session lifetime, in seconds.
     */
    const SETTING_SESSION_LIFETIME = 'RESURSBANK_SESSION_LIFETIME';

    /**
     * Payment methods that can be configured with part payment options.
     */
    const SETTING_PARTPAYMENT_METHOD = 'RESURSBANK_PARTPAYMENT_METHOD';

    /**
     * Duration to use with part payment widget (12 months, 24 months, etc). Dynamically fetched.
     */
    const SETTING_PARTPAYMENT_DURATION = 'RESURSBANK_PARTPAYMENT_DURATION';

    /**
     * Interval in minutes how old unprocessed orders can be before cancelling them.
     */
    const SETTING_CLEANER_INTERVAL = 'RESURSBANK_CLEANER_INTERVAL';

    /**
     * When this is enabled, we do not starting sessions ourselves. We lay our trust in that someone
     * else will do this.
     */
    //const SETTING_SKIP_SESSION_START = 'RESURSBANK_SKIP_SESSION_START';


    /**
     * The ResursBank plugin module
     *
     * @var ResursBankCheckout
     */
    protected $module = null;

    /**
     * Has the form been saved successfully?
     *
     * @var boolean
     */
    protected $success = false;

    /**
     * Property to hold errors that has happened.
     *
     * @var array
     */
    protected $errors = [];


    public function __construct($module)
    {
        $this->module = $module;
    }

    /**
     * Check if the the config form was submitted in the current request.
     *
     * @return bool
     */
    public function submitted()
    {
        return (bool)Tools::isSubmit(self::CONFIG_SUBMIT_ACTION);
    }

    /**
     * Check if the form request validates.
     *
     * @return bool
     * @throws Exception
     */
    public function validates()
    {
        if (!(bool)Tools::getValue(self::SETTING_ACTIVE)) {
            return true;
        }

        if ((bool)Tools::getValue(self::SETTING_ENVIRONMENT)) {
            $this->validateProduction();
        } else {
            $this->validateTest();
        }

        return !$this->hasErrors();
    }

    /**
     * Save configuration values.
     *
     * @return void
     */
    public function save()
    {
        foreach ($this->formKeys() as $key) {
            Configuration::updateValue($key, Tools::getValue($key, Configuration::get($key)));
        }

        $this->success = true;
    }

    /**
     * Render configuration page.
     *
     * @return string
     * @throws Exception
     */
    public function render()
    {
        // Assign template variables.
        $this->module->getContext()->smarty->assign(
            [
                'module_enabled' => $this->getProperEnabledValue($this->module),
                'module_dir' => $this->module->getPath(),
                'version' => ResursBankCheckout::VERSION,
                'ecom' => defined('ECOMPHP_VERSION') ? ECOMPHP_VERSION : '',
                'currentEnvironment' => (bool)Configuration::get(self::SETTING_ENVIRONMENT) ? 'production' : 'test',
                'callbacks' => $this->module->getApi()->getCallbackHandler()->getRegisteredCallbacks(),
                'adminControllerUrl' => $this->module->getContext()->link->getAdminLink('AdminResursbankcheckout'),
                'paymentMethods' => ResursBankCheckout\Models\Service\Payment::getMethods(),
                'paymentMethodsAnnuity' => ResursBankCheckout\Models\Service\Payment::getAnnuity(),
                'currentAnnuityMethod' => Configuration::get(self::SETTING_PARTPAYMENT_METHOD),
                'currentAnnuityDuration' => $this->getConfiguredPartPaymentDuration(),
            ]
        );

        // Render configuration page.
        return (
            $this->getRcoUrlRewrite() .
            $this->renderNotification() .
            $this->renderForm() .
            $this->module->renderView('admin/configure_bottom.tpl')
        );
    }

    /**
     * Generate an error message if the url_rewrite for order is wrong.
     * @return string|null
     * @throws Exception
     */
    private function getRcoUrlRewrite()
    {
        $return = null;
        try {
            $langTbl = _DB_PREFIX_ . "meta_lang";
            $sqlQuery = "SELECT url_rewrite FROM " . _DB_PREFIX_ . "meta_lang " . "WHERE title = 'Beställning' AND url_rewrite = 'För'";
            $languageError = Db::getInstance()->executeS($sqlQuery);

            if (count($languageError) > 0) {
                $this->module->getContext()->smarty->assign(
                    [
                        'errormessage' => $this->module->l("We have found a known issue in your platform database ($langTbl) that potentially could cause problems with loading Resurs Checkout."),
                        'fixit' => $this->module->l("To fix this issue, use the SQL query below to find it. When found, make sure you change the field that contains 'För' to for example 'order'."),
                        'sqlQuery' => $sqlQuery,
                    ]
                );

                $return = $this->module->renderView('admin/configure_rewrite.tpl');
            }
        } catch (Exception $e) {
            // If something fails on this run, do not make this a showstopper.
        }

        return $return;
    }

    /**
     * Gets the proper configured duration for annuityfactors. If none is set, a default value will be set
     * so that the partpayment widget can show properly.
     *
     * @return string
     */
    private function getConfiguredPartPaymentDuration()
    {
        $return = Configuration::get(self::SETTING_PARTPAYMENT_DURATION);
        $currentMethod = Configuration::get(self::SETTING_PARTPAYMENT_METHOD);
        $factors = ResursBankCheckout\Models\Service\Payment::getAnnuity();

        if ($currentMethod !== '' &&
            empty($return) &&
            isset($factors[$currentMethod]) &&
            is_array($factors[$currentMethod]) &&
            count($factors[$currentMethod])
        ) {
            // Default duration is the first duration value found in a reverse array and will be stored if nothing is
            // stored earlier before. Reversing and popping arrays usually may cause conflicts so this is split up.
            $reverseDuration = array_reverse($factors[$currentMethod]);
            $durationData = array_pop($reverseDuration);
            $return = $durationData['duration'];
            Configuration::updateValue(self::SETTING_PARTPAYMENT_DURATION, $return);
        }

        return $return;
    }

    /**
     * Check if module is enabled.
     *
     * @param ResursBankCheckout $module
     * @return bool
     */
    private function getProperEnabledValue(ResursBankCheckout $module)
    {
        $getConfiguredValue = $module->enabled();
        $getPostedValue = Tools::getValue(self::SETTING_ACTIVE);
        if (!$getConfiguredValue && $getPostedValue) {
            $result = true;
        } else {
            $result = $getConfiguredValue;
        }

        return $result;
    }

    /**
     * Render configuration form.
     *
     * @return string
     * @throws \PrestaShopException
     */
    private function renderForm()
    {
        // Render and return form HTML.
        return $this->getFormHelper()->generateForm([
            $this->getConfigForm(),
        ]);
    }

    /**
     * Retrieve form helper.
     *
     * @return \HelperFormCore
     * @throws \PrestaShopException
     */
    private function getFormHelper()
    {
        /** @var \HelperFormCore formHelper */
        $result = new HelperForm();
        $result->show_toolbar = false;
        $result->module = $this->module;
        $result->default_form_language = $this->module->getContext()->language->id;
        $result->allow_employee_form_lang = (int)Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG');
        $result->submit_action = self::CONFIG_SUBMIT_ACTION;
        $result->token = Tools::getAdminTokenLite('AdminModules');

        $result->currentIndex = $this->module->getContext()->link->
            getAdminLink(
                'AdminModules',
                false
            ) .
            '&configure=' . $this->module->getName()
            . '&tab_module=' . self::TAB
            . '&module_name=' . $this->module->getName();

        // Assign template variables, such as the value of the config fields.
        $result->tpl_vars = [
            'fields_value' => $this->formValues(),
            'languages' => $this->module->getContext()->controller->getLanguages(),
            'id_language' => $this->module->getContext()->language->id,
        ];

        return $result;
    }

    /**
     * Retrieve configuration form structure.
     *
     * @return array
     */
    private function getConfigForm()
    {
        return [
            'form' => [
                'legend' => [
                    'title' => $this->module->l('Settings'),
                    'icon' => 'icon-cogs',
                ],
                'input' => [
                    [
                        'type' => 'switch',
                        'label' => $this->module->l('Active'),
                        'name' => self::SETTING_ACTIVE,
                        'is_bool' => true,
                        'values' => [
                            [
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->module->l('Enabled'),
                            ],
                            [
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->module->l('Disabled'),
                            ],
                        ],
                    ],
                    [
                        'type' => 'switch',
                        'label' => $this->module->l('Debug Enabled'),
                        'name' => self::SETTING_DEBUG_ENABLED,
                        'is_bool' => true,
                        'values' => [
                            [
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->module->l('Enabled'),
                            ],
                            [
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->module->l('Disabled'),
                            ],
                        ],
                    ],
                    [
                        'type' => 'switch',
                        'label' => $this->module->l('Round tax percentage'),
                        'name' => self::SETTING_ROUND_TAX_PERCENTAGE,
                        'is_bool' => true,
                        'desc' => $this->module->l('Round tax percentage values to nearest integer.'),
                        'values' => [
                            [
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->module->l('Enabled'),
                            ],
                            [
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->module->l('Disabled'),
                            ],
                        ],
                    ],
                    [
                        'type' => 'switch',
                        'label' => $this->module->l('Store SSN'),
                        'name' => self::SETTING_STORE_SSN,
                        'is_bool' => true,
                        'desc' => $this->module->l('Store SSN on billing address.'),
                        'values' => [
                            [
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->module->l('Enabled'),
                            ],
                            [
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->module->l('Disabled'),
                            ],
                        ],
                    ],
                    [
                        'type' => 'switch',
                        'label' => $this->module->l('Production'),
                        'name' => self::SETTING_ENVIRONMENT,
                        'is_bool' => true,
                        'desc' => $this->module->l('Defines if the payment module is set to production mode'),
                        'values' => [
                            [
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->module->l('Enabled'),
                            ],
                            [
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->module->l('Disabled'),
                            ],
                        ],
                    ],
                    /*[
                        'col' => 3,
                        'type' => 'switch',
                        'name' => self::SETTING_SKIP_SESSION_START,
                        'is_bool' => true,
                        'label' => $this->module->l('Skip session initializations'),
                        'desc' => $this->module->l('This module will not initialize session if enabled. We lay our trust in that someone else does this.'),
                        'values' => [
                            [
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->module->l('Enabled'),
                            ],
                            [
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->module->l('Disabled'),
                            ],
                        ],
                    ],*/
                    [
                        'col' => 3,
                        'type' => 'text',
                        'name' => self::SETTING_PRODUCTION_USERNAME,
                        'label' => $this->module->l('Username for production'),
                    ],
                    [
                        'col' => 3,
                        'type' => 'text',
                        'name' => self::SETTING_PRODUCTION_PASSWORD,
                        'label' => $this->module->l('Password for production'),
                    ],
                    [
                        'col' => 3,
                        'type' => 'text',
                        'name' => self::SETTING_TEST_USERNAME,
                        'label' => $this->module->l('Username for test/staging'),
                    ],
                    [
                        'col' => 3,
                        'type' => 'text',
                        'name' => self::SETTING_TEST_PASSWORD,
                        'label' => $this->module->l('Password for test/staging'),
                    ],
                    [
                        'col' => 3,
                        'type' => 'text',
                        'name' => self::SETTING_SESSION_LIFETIME,
                        'label' => $this->module->l('Session lifetime (in seconds)'),
                    ],
                    [
                        'col' => 3,
                        'type' => 'text',
                        'name' => self::SETTING_CLEANER_INTERVAL,
                        'label' => $this->module->l('Cleaner interval'),
                        'desc' => $this->module->l('Interval set in minutes on expired orders (orders that for example does not exist at Resurs Bank or where the status has not changed). Default is 1 hour.'),
                    ],
                ],
                'submit' => [
                    'title' => $this->module->l('Save'),
                ],
            ],
        ];
    }


    /**
     * Validate the form submission when environment is set to production.
     *
     * @return void
     * @throws Exception
     */
    private function validateProduction()
    {
        if (empty(Tools::getValue(self::SETTING_PRODUCTION_USERNAME))) {
            $this->errors[] = $this->module->l('Username for production is required when the production environment is set.');
        }

        if (empty(Tools::getValue(self::SETTING_PRODUCTION_PASSWORD))) {
            $this->errors[] = $this->module->l('Password for production is required when the production environment is set.');
        }

        if (!$this->hasErrors()) {
            if (!$this->module->getApi()->checkCredentials(
                true,
                Tools::getValue(self::SETTING_PRODUCTION_USERNAME),
                Tools::getValue(self::SETTING_PRODUCTION_PASSWORD)
            )) {
                $this->errors[] = $this->module->l('The production credentials are not valid.');
            }
        }
    }

    /**
     * Validate the form submission when environment is set to test.
     *
     * @return void
     * @throws Exception
     */
    private function validateTest()
    {
        if (empty(Tools::getValue(self::SETTING_TEST_USERNAME))) {
            $this->errors[] =
                $this->module->l('Username for test/staging is required when the test environment is set.');
        }

        if (empty(Tools::getValue(self::SETTING_TEST_PASSWORD))) {
            $this->errors[] =
                $this->module->l('Password for test/staging is required when the test environment is set.');
        }

        if (!$this->hasErrors()) {
            if (!$this->module->getApi()->checkCredentials(
                false,
                Tools::getValue(self::SETTING_TEST_USERNAME),
                Tools::getValue(self::SETTING_TEST_PASSWORD)
            )) {
                $this->errors[] = $this->module->l('The test/staging credentials are not valid.');
            }
        }
    }

    /**
     * Render notifications for the form.
     *
     * @return string
     */
    private function renderNotification()
    {
        if ($this->hasErrors()) {
            return $this->module->displayError($this->errors);
        }

        if ($this->success) {
            return $this->module->displayConfirmation(
                $this->module->l('Successfully saved the settings.')
            );
        }

        return '';
    }

    /**
     * Does the form have any errors?
     *
     * @return boolean
     */
    private function hasErrors()
    {
        return count($this->errors) > 0;
    }

    /**
     * Return all of the formkeys in an array.
     *
     * @return array
     */
    private function formKeys()
    {
        return [
            self::SETTING_ACTIVE,
            self::SETTING_ENVIRONMENT,
            self::SETTING_PRODUCTION_USERNAME,
            self::SETTING_PRODUCTION_PASSWORD,
            self::SETTING_TEST_USERNAME,
            self::SETTING_TEST_PASSWORD,
            self::SETTING_DEBUG_ENABLED,
            self::SETTING_STORE_SSN,
            self::SETTING_ROUND_TAX_PERCENTAGE,
            self::SETTING_SESSION_LIFETIME,
            self::SETTING_PARTPAYMENT_METHOD,
            self::SETTING_PARTPAYMENT_DURATION,
            self::SETTING_PAYMENT_METHODS,
            self::SETTING_ANNUITY_FACTORS,
            self::SETTING_CLEANER_INTERVAL,
        ];
    }

    /**
     * Get the form values to be shown on the view.
     *
     * @return array
     */
    private function formValues()
    {
        $values = [];

        foreach ($this->formKeys() as $key) {
            $values[$key] = Tools::getValue($key, Configuration::get($key));
        }

        return $values;
    }

    /**
     * Delete all form values from database (useful during the uninstall process.)
     */
    public function deleteAllFormValues()
    {
        foreach ($this->formKeys() as $key) {
            Configuration::deleteByName($key);
        }
    }

    /**
     * Whether or not to store SSN on billing address.
     *
     * @return bool
     */
    public static function getStoreSsn()
    {
        return ((int)Configuration::get(self::SETTING_STORE_SSN) === 1);
    }

    /**
     * Whether or not to round tax percentage values.
     *
     * @return bool
     */
    public static function getRoundTaxPercentage()
    {
        return ((int)Configuration::get(self::SETTING_ROUND_TAX_PERCENTAGE) === 1);
    }
}
