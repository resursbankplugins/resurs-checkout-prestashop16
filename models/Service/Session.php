<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace ResursBankCheckout\Models\Service;

use \Configuration;

/**
 * Class Session
 *
 * General session driver.
 *
 * @package ResursBankCheckout\Models\Service
 */
abstract class Session
{
    /**
     * Session key name prefix.
     */
    const KEY_PREFIX = 'resursbank_checkout_';

    /**
     * Start session.
     */
    public static function start()
    {
        // Session lifetime, in seconds.
        $lifetime = self::getLifetime();

        // Set ini options.
        self::setIniOptions($lifetime);

        // Start session handler.
        if (session_id() === "") {
            session_start();
        }

        // Internal garbage collection routine.
        self::garbageCollection($lifetime);

        // Set session lifetime.
        if (
            $lifetime > 0 &&
            !isset($_SESSION['ttl'])
        ) {
            // Set local garbage collection timeout.
            $_SESSION['ttl'] = time() + $lifetime;
        }
    }

    /**
     * Set ini options.
     *
     * @param $lifetime
     */
    private static function setIniOptions($lifetime)
    {
        $lifetime = (int) $lifetime;

        // Set session file expiration time (garbage collection).
        ini_set('session.gc_maxlifetime', $lifetime);

        // Set strict mode.
        ini_set('session.use_strict_mode', true);

        // Set client side session id storage expiration time.
        session_set_cookie_params($lifetime);
    }

    /**
     * Perform internal garbage collection.
     *
     * @param int $lifetime
     */
    private static function garbageCollection($lifetime)
    {
        $lifetime = (int) $lifetime;

        if (
            $lifetime > 0 &&
            isset($_SESSION['ttl']) &&
            time() > (int) $_SESSION['ttl']
        ) {
            // Unset session data.
            session_unset();

            // Destroy session.
            session_destroy();

            // Restart session handler.
            self::start();
        }
    }

    /**
     * Retrieve session lifetime.
     *
     * @return int
     */
    public static function getLifetime()
    {
        return (int) Configuration::get(
            \ResursBankCheckout\Models\Service\Config::SETTING_SESSION_LIFETIME
        );
    }

    /**
     * Store value in session.
     *
     * @param string $key
     * @param mixed $val
     */
    public static function set($key, $val)
    {
        $_SESSION[self::getKey($key)] = $val;
    }

    /**
     * Check if a value is set in session.
     *
     * @param string $key
     * @return bool
     */
    public static function has($key)
    {
        return isset($_SESSION[self::getKey($key)]);
    }

    /**
     * Retrieve value from session.
     *
     * @param string $key
     * @return mixed
     */
    public static function get($key)
    {
        $result = null;

        if (self::has($key)) {
            $result = $_SESSION[self::getKey($key)];
        }

        return $result;
    }

    /**
     * Unset key in session.
     *
     * @param string $key
     */
    public static function uns($key)
    {
        if (self::has($key)) {
            unset($_SESSION[self::getKey($key)]);
        }
    }

    /**
     * Retrieve properly formatted key name.
     *
     * @param string $key
     * @return string
     */
    public static function getKey($key)
    {
        return self::KEY_PREFIX . (string) $key;
    }
}
