<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace ResursBankCheckout\Models\Service;

use \Exception;
use \Tools;
use \Customer as PrestaCustomer;
use \ResursBankCheckout\Models\Service\Customer\Validate;
use \ResursBankCheckout\Models\Service\Customer\Address\Billing;

/**
 * Class Customer
 *
 * Customer service class. Contains methods to handle customers.
 *
 * @package ResursBankCheckout\Models\Service
 */
abstract class Customer
{
    /**
     * Session key where customer data is stored.
     */
    const SESSION_DATA_KEY = 'resursbank_checkout_data_customer';

    /**
     * Load customer by email.
     *
     * @param string $email
     * @return PrestaCustomer
     */
    public static function getByEmail($email)
    {
        $result = new PrestaCustomer();
        $result->getByEmail((string) $email, null, false);

        return $result;
    }

    /**
     * The data argument should include "firstname", "lastname" and "email" (all of which are strings).
     *
     * @param array $data
     * @return PrestaCustomer
     * @throws Exception
     */
    public static function create(array $data)
    {
        $customer = new PrestaCustomer();

        // Validate customer data.
        self::validate($data);

        // Assign data to Customer object.
        self::assignData($customer, $data);

        // Create customer.
        $customer->add();

        return $customer;
    }

    /**
     * Assign data to customer object.
     *
     * @param PrestaCustomer $customer
     * @param array $data
     */
    private static function assignData(
        PrestaCustomer &$customer,
        array $data
    ) {
        $customer->firstname = (string) $data['firstname'];
        $customer->lastname =  (string) $data['lastname'];
        $customer->email = (string) $data['email'];

        $customer->passwd = Tools::encrypt(
            Tools::passwdGen(8)
        );

        $customer->is_guest = 1;
    }

    /**
     * Handle customer data request. This is basically executed when we save our billing address, so the request data
     * comes from the same source.
     *
     * @return bool
     * @throws Exception
     */
    public static function handleDataRequest()
    {
        // Retrieve data from HTTP request.
        $data = self::getRequestData();

        // Clean request data.
        self::cleanRequestData($data, Validate::getRequiredProperties());

        // Validate user input.
        self::validate($data);

        // Store data in session.
        self::setSessionData($data);

        return true;
    }

    /**
     * Retrieve billing address data from request. This is used to collect our basic customer data.
     *
     * @return array
     * @throws Exception
     */
    public static function getRequestData()
    {
        return Billing::getRequestData();
    }

    /**
     * Short-hand to validation method.
     *
     * @param array $data
     * @return bool
     * @throws Exception
     */
    protected static function validate(array $data)
    {
        return Validate::validate($data);
    }

    /**
     * Retrieve address data from session.
     *
     * @param string $key
     * @return mixed
     * @throws Exception
     */
    public static function getSessionData($key = '')
    {
        $result = Session::get(self::SESSION_DATA_KEY);

        if (!is_array($result)) {
            $result = array();
        }

        if (is_string($key) && $key !== '') {
            if (!isset($result[$key])) {
                throw new Exception('Missing customer property ' . $key);
            }

            $result = $result[$key];
        }

        return $result;
    }

    /**
     * Clean up request data object.
     *
     * @param array $data
     * @param array $allowedProperties
     */
    private static function cleanRequestData(
        array &$data,
        array $allowedProperties
    ) {
        foreach ($data as $key => $val) {
            if (!in_array($key, $allowedProperties)) {
                unset($data[$key]);
            }
        }
    }

    /**
     * Load billing data from session and apply on this instance.
     *
     * @param array $data
     */
    private static function setSessionData(array $data)
    {
        Session::set(self::SESSION_DATA_KEY, $data);
    }

    /**
     * Clear session data.
     */
    public static function clearSessionData()
    {
        Session::uns(self::SESSION_DATA_KEY);
    }
}
