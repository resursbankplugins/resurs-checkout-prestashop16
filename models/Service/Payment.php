<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace ResursBankCheckout\Models\Service;

use \Exception;
use \Cart as PrestaCart;
use \Configuration;
use \ResursBankCheckout;
use \ResursBankCheckout\Models\Service\Cart;
use \ResursBankCheckout\Models\Service\Session;
use \ResursBankCheckout\Models\Service\Order;
use \ResursBankCheckout\Models\Service\Config;

/**
 * Class Payment
 *
 * Payment service class. Contains methods to handle payment methods and payment operations.
 *
 * @package ResursBankCheckout\Models\Service
 */
abstract class Payment
{
    /**
     * Session key where payment method data is stored.
     */
    const SESSION_DATA_KEY = 'resursbank_checkout_data_payment_method';

    /**
     * Handle payment method selection request.
     *
     * @return bool
     * @throws Exception
     */
    public static function handleDataRequest()
    {
        // Retrieve data from HTTP request.
        $data = self::getRequestData();

        // Validate user input.
        self::validate($data);

        // Store data in session.
        self::setSessionData($data);

        return true;
    }

    /**
     * Retrieve payment method from request.
     *
     * @return string
     * @throws Exception
     */
    public static function getRequestData()
    {
        $data = isset($_REQUEST['payment']) ? $_REQUEST['payment'] : null;

        if (!is_string($data)) {
            throw new Exception('Payment data is not a string.');
        }

        $data = @json_decode($data, true);

        if (!is_array($data)) {
            throw new Exception('Payment data is not an array.');
        }

        if (!isset($data['method'])) {
            throw new Exception('Missing method in payment data.');
        }

        return self::getMethod($data['method']);
    }

    /**
     * Retrieve billing data from session.
     *
     * @return mixed
     * @throws Exception
     */
    public static function getSessionData()
    {
        return Session::get(self::SESSION_DATA_KEY);
    }

    /**
     * Load billing data from session and apply on this instance.
     *
     * @param string $data
     */
    private static function setSessionData($data)
    {
        Session::set(self::SESSION_DATA_KEY, $data);
    }

    /**
     * Clear session data.
     */
    public static function clearSessionData()
    {
        Session::uns(self::SESSION_DATA_KEY);
    }

    /**
     * Validate submitted payment method.
     *
     * @param string $method
     * @return bool
     * @throws Exception
     */
    private static function validate($method)
    {
        if (!is_string($method)) {
            throw new Exception('Invalid data type for payment method.');
        }

        return true;
    }

    /**
     * Invoked by failure controller. This method will rebuild cart contents using an old cart (after validating that
     * it belongs to the customer attempting to recreate it, using secure_key) and cancel the previous order created
     * before payment began.
     *
     * @param ResursBankCheckout $module
     * @param int $cartId
     * @param string $validationKey
     * @return bool
     * @throws Exception
     */
    public static function handleFailedPayment(
        ResursBankCheckout $module,
        $cartId,
        $validationKey
    ) {
        $cartId = (int)$cartId;

        if ($cartId === 0) {
            throw new Exception('Failed to rebuild cart. No cart id provided.');
        }

        /** @var PrestaCart $cart */
        $cart = new PrestaCart($cartId);

        // Validate cart object.
        if (!Cart::validateCartObject($cart)) {
            throw new Exception('Failed to load cart from id ' . $cartId);
        }

        // Validate cart ownership.
        if (Cart::getValidationKey($cart) !== (string)$validationKey) {
            throw new Exception('Invalid validation key.');
        }

        // Cancel previous order.
        Order::cancel(Order::getByCart($cart));

        // Rebuild cart.
        Cart::rebuild($module, $cart);

        return true;
    }

    /**
     * Retrieve payment method title by code.
     *
     * @param string $id
     * @return string
     */
    public static function getMethod($id)
    {
        $id = strtolower((string)$id);

        $methods = self::getMethods();

        return (
            isset($methods[$id]) &&
            is_array($methods[$id]) &&
            isset($methods[$id]['description'])
        ) ? (string)$methods[$id]['description'] : '';
    }

    /**
     * Retrieve array of all available payment methods stored in our database.
     *
     * @return array
     */
    public static function getMethods()
    {
        $result = @json_decode(self::getRawMethodData(), true);

        return is_array($result) ? $result : array();
    }

    /**
     * Transform raw data of annuity factors into array.
     *
     * @return array
     */
    public static function getAnnuity()
    {
        $result = @json_decode(self::getRawAnnuityData(), true);

        return is_array($result) ? $result : array();
    }

    /**
     * Get list of payment methods (payment method objects), that support annuity factors.
     * Modification from EComPHP 1.3.9.
     *
     * @param bool $namesOnly
     * @return array
     */
    public static function getMethodsByAnnuity($namesOnly = false)
    {
        $allMethods = self::getMethods();
        $annuityMethods = array();
        if (is_array($allMethods) && count($allMethods)) {
            $annuitySupported = array('REVOLVING_CREDIT');
            foreach ($allMethods as $methodIndex => $methodObject) {
                $t = isset($methodObject['type']) ? $methodObject['type'] : null;
                $s = isset($methodObject['specificType']) ? $methodObject['specificType'] : null;
                if (in_array($t, $annuitySupported) || in_array($s, $annuitySupported)) {
                    if (!$namesOnly) {
                        $annuityMethods[] = $methodObject;
                    } else {
                        if (isset($methodObject->id)) {
                            $annuityMethods[] = $methodObject['id'];
                        }
                    }
                }
            }
        }

        return $annuityMethods;
    }

    /**
     * Get raw JSON encoded value describing all available payment methods.
     *
     * @return string
     */
    public static function getRawMethodData()
    {
        return (string)Configuration::get(Config::SETTING_PAYMENT_METHODS);
    }

    /**
     * Get raw JSON encoded value with payment methods that supports annuity factors, and the factors.
     *
     * @return string
     */
    public static function getRawAnnuityData()
    {
        return (string)Configuration::get(Config::SETTING_ANNUITY_FACTORS);
    }

    /**
     * Update payment methods from API.
     *
     * @param ResursBankCheckout $module
     * @throws Exception
     */
    public static function updateMethods(ResursBankCheckout $module)
    {
        // Keeping until we see that this gives the right effect everywhere.
        //$methods = self::getMethods();
        $methods = array();
        $newMethods = self::fetchMethodsFromApi($module);

        if (count($newMethods)) {
            foreach ($newMethods as $id => $data) {
                $methods[$id] = $data;
            }
        }

        self::setRawMethodData($methods);
    }

    /**
     * Update annuity factors for supported methods from API.
     *
     * @param ResursBankCheckout $module
     */
    public static function updateAnnuity(ResursBankCheckout $module)
    {
        $annuityFactors = self::fetchAnnuityFactorsFromApi($module);

        self::setRawAnnuityData($annuityFactors);
    }

    /**
     * Fetch payment methods from API.
     *
     * @param ResursBankCheckout $module
     * @return array
     * @throws Exception
     */
    public static function fetchMethodsFromApi(ResursBankCheckout $module)
    {
        $result = array();

        $methods = $module->getApi()->getPaymentMethods();

        if (is_array($methods) && count($methods)) {
            foreach ($methods as $method) {
                $id = strtolower((string)$method->id);

                $result[$id] = (array)$method;
                $result[$id]['id'] = $id;
            }
        }

        return $result;
    }

    /**
     * Pick up annuity factors with help from API.
     *
     * @param ResursBankCheckout $module
     * @return array
     */
    public static function fetchAnnuityFactorsFromApi(ResursBankCheckout $module)
    {
        $result = array();
        $annuityMethods = self::getMethodsByAnnuity();

        if (is_array($annuityMethods) && count($annuityMethods)) {
            foreach ($annuityMethods as $method) {
                try {
                    $result[$method['id']] = $module->getApi()->getAnnuityFactors($method['id']);
                } catch (Exception $e) {
                    // Silently skip failures
                }
            }
        }

        return $result;
    }

    /**
     * Update the value in our database containing a list of all available payment methods.
     *
     * @param array $data
     */
    public static function setRawMethodData(array $data)
    {
        Configuration::updateValue(Config::SETTING_PAYMENT_METHODS, @json_encode($data));
    }

    /**
     * Update the value in our database containing a list of annuity factors for supported payment methods.
     *
     * @param array $data
     */
    public static function setRawAnnuityData(array $data)
    {
        Configuration::updateValue(Config::SETTING_ANNUITY_FACTORS, @json_encode($data));
    }
}
