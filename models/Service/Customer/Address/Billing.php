<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace ResursBankCheckout\Models\Service\Customer\Address;

use Address as PrestaAddress;
use Customer as PrestaCustomer;
use Exception;
use HookCore;
use Order as PrestaOrder;
use ResursBankCheckout;
use ResursBankCheckout\Models\Service\Config;
use ResursBankCheckout\Models\Service\Customer\Address;
use ResursBankCheckout\Models\Service\Session;

/**
 * Class Billing
 *
 * Customer billing address service class. Extends the parent Address class to make use of its common methods. Contains
 * billing address specific methods/information.
 *
 * @package ResursBankCheckout\Models\Service\Customer\Address
 */
abstract class Billing extends Address
{
    /**
     * Session key where billing address data is stored.
     */
    const SESSION_DATA_KEY = 'resursbank_checkout_data_billing_address';

    /**
     * Session key where we store a flag indicating whether or not the billing address will also be used for shipping.
     */
    const SESSION_USE_FOR_SHIPPING_KEY = 'resursbank_checkout_data_billing_address_use_for_shipping';

    /**
     * Billing address alias.
     */
    const ALIAS = 'Folkbokföringsadress';

    /**
     * Handle billing data request. When a user submits billing data to us (normally through the controller
     * "savebilling") we use this method to handle that request, validating and storing the submitted data in the user
     * session.
     *
     * @return bool
     * @throws Exception
     */
    public static function handleDataRequest()
    {
        // Retrieve data from HTTP request.
        $data = self::getRequestData();

        if (isset($data['use_for_shipping'])) {
            self::setUseForShipping((int) $data['use_for_shipping'] === 1);
        }

        // Clean request data.
        parent::cleanRequestData($data, self::getAllowedProperties($data));

        // Validate user input.
        self::validate($data);

        // Store data in session.
        self::setSessionData($data);

        return true;
    }

    /**
     * Retrieve all allowed address properties.
     *
     * @param array $requestData
     * @return array
     */
    private static function getAllowedProperties(array $requestData)
    {
        $result = Validate::getRequiredProperties();

        if (isset($requestData['ssn'])) {
            $result[] = 'ssn';
        }

        return $result;
    }

    /**
     * Retrieve billing address data from request.
     *
     * @return array
     * @throws Exception
     */
    public static function getRequestData()
    {
        $data = isset($_REQUEST['billing']) ? $_REQUEST['billing'] : null;

        if (!is_string($data)) {
            throw new Exception('Billing data is not a string.');
        }

        $data = @json_decode($data, true);

        if (!is_array($data)) {
            throw new Exception('Billing data is not an array.');
        }

        return $data;
    }

    /**
     * Retrieve billing data from session.
     *
     * @param string $key
     * @return mixed
     * @throws Exception
     */
    public static function getSessionData($key = '')
    {
        return parent::getSessionSubData(self::SESSION_DATA_KEY, $key);
    }

    /**
     * Clear session data.
     */
    public static function clearSessionData()
    {
        Session::uns(self::SESSION_DATA_KEY);
    }

    /**
     * Load billing data from session and apply on this instance.
     *
     * @param array $data
     */
    private static function setSessionData(array $data)
    {
        Session::set(self::SESSION_DATA_KEY, $data);
    }

    /**
     * Whether or not to use the billing address for shipping.
     *
     * @return bool
     */
    public static function useForShipping()
    {
        return (bool) Session::get(self::SESSION_USE_FOR_SHIPPING_KEY);
    }

    /**
     * Flag that we intend to use billing address for shipping.
     *
     * @param bool $val
     */
    public static function setUseForShipping($val)
    {
        Session::set(self::SESSION_USE_FOR_SHIPPING_KEY, (bool) $val);
    }

    /**
     * Assign address data.
     *
     * @param PrestaAddress $address
     * @param PrestaCustomer $customer
     * @param array $data
     */
    public static function assignData(
        PrestaAddress &$address,
        PrestaCustomer $customer,
        array $data
    ) {
        parent::assignData($address, $customer, $data);

        $address->alias = self::ALIAS;
    }

    /**
     * Check if provided address instance is missing a vat_number value.
     *
     * @param PrestaAddress $address
     * @return bool
     */
    public static function isMissingSsn(PrestaAddress $address)
    {
        return isset($address->vat_number) && $address->vat_number === '';
    }

    /**
     * Update SSN on billing address if it's missing. This performs an API call
     * to retrieve the order payment from Resurs Bank and collect the SSN from
     * there. The SSN will only be missing if it wasn't filled out to collect
     * customer address information during the checkout process.
     *
     * @param ResursBankCheckout $module
     * @param PrestaOrder $order
     * @throws Exception
     */
    public static function updateMissingSsn(
        ResursBankCheckout $module,
        PrestaOrder $order
    ) {
        if (Config::getStoreSsn()) {
            /** @var PrestaAddress $billingAddress */
            $billingAddress = new PrestaAddress($order->id_address_invoice);

            if (self::isMissingSsn($billingAddress)) {
                $billingAddress->vat_number = $module->getApi()->getPaymentSsn(
                    $order->reference
                );

                // First time hook execution.
                HookCore::exec(
                    'UpdateResursSsn',
                    [
                        'order' => $order,
                        'vat_number' => $billingAddress->vat_number,
                    ]
                );

                $billingAddress->update();
            }
        }
    }
}
