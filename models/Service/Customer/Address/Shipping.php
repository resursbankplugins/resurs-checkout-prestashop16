<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace ResursBankCheckout\Models\Service\Customer\Address;

use \Address as PrestaAddress;
use \Customer as PrestaCustomer;
use \Exception;
use \ResursBankCheckout\Models\Service\Session;
use \ResursBankCheckout\Models\Service\Customer\Address;

/**
 * Class Shipping
 *
 * Customer shipping address service class. Extends the parent Address class to make use of its common methods. Contains
 * shipping address specific methods/information.
 *
 * @package ResursBankCheckout\Models\Service\Customer\Address
 */
abstract class Shipping extends Address
{
    /**
     * Session key where shipping address data is stored.
     */
    const SESSION_DATA_KEY = 'resursbank_checkout_data_shipping_address';

    /**
     * Address type alias.
     */
    const ALIAS = 'Leveransadress';

    /**
     * Handle shipping data request. When a user submits shipping data to us (normally through the controller
     * "saveshipping") we use this method to handle that request, validating and storing the submitted data in the user
     * session.
     *
     * @param string $telephone (this is not included in the iframe form, so we need to provide it)
     * @return bool
     * @throws Exception
     */
    public static function handleDataRequest($telephone)
    {
        // Retrieve data from HTTP request.
        $data = self::getRequestData();

        // Add telephone property.
        $data['telephone'] = (string) $telephone;

        // Clean request data.
        parent::cleanRequestData($data, Validate::getRequiredProperties());

        // Validate user input.
        self::validate($data);

        // Store data in session.
        self::setSessionData($data);

        return true;
    }

    /**
     * Retrieve shipping address data from request.
     *
     * @return array
     * @throws Exception
     */
    public static function getRequestData()
    {
        $data = isset($_REQUEST['shipping']) ? $_REQUEST['shipping'] : null;

        if (!is_string($data)) {
            throw new Exception('Shipping data is not a string.');
        }

        $data = @json_decode($data, true);

        if (!is_array($data)) {
            throw new Exception('Shipping data is not an array.');
        }

        return $data;
    }

    /**
     * Retrieve shipping data from session.
     *
     * @param string $key
     * @return mixed
     * @throws Exception
     */
    public static function getSessionData($key = '')
    {
        return parent::getSessionSubData(self::SESSION_DATA_KEY, $key);
    }

    /**
     * Load shipping data from session and apply on this instance.
     *
     * @param array $data
     */
    private static function setSessionData(array $data)
    {
        Session::set(self::SESSION_DATA_KEY, $data);
    }

    /**
     * Clear session data.
     */
    public static function clearSessionData()
    {
        Session::uns(self::SESSION_DATA_KEY);
    }

    /**
     * Assign address data.
     *
     * @param PrestaAddress $address
     * @param PrestaCustomer $customer
     * @param array $data
     */
    public static function assignData(
        PrestaAddress &$address,
        PrestaCustomer $customer,
        array $data
    ) {
        parent::assignData($address, $customer, $data);

        $address->alias = self::ALIAS;
    }
}
