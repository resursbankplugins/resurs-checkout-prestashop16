<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace ResursBankCheckout\Models\Service\Order;

use \ResursBankCheckout;
use \Language;
use \Db;
use \Configuration;
use \Exception;
use \OrderState as PrestaOrderState;

/**
 * Class State
 *
 * Order state service class. Contains methods to handle order states and order state translations.
 *
 * @package ResursBankCheckout\Models\Service\Order
 */
abstract class State
{
    /**
     * Custom order status indicating pending order payment.
     */
    const PENDING = 'RESURSBANKCHECKOUT_STATUS_PENDING';

    /**
     * Custom order status indicating processing order payment.
     */
    const PROCESSING = 'RESURSBANKCHECKOUT_STATUS_PROCESSING';

    /**
     * Custom order status indicating completed order payment.
     */
    const COMPLETED = 'RESURSBANKCHECKOUT_STATUS_COMPLETED';

    /**
     * Custom order status indicating suspected fraud.
     */
    const SUSPECTED_FRAUD = 'RESURSBANKCHECKOUT_STATUS_SUSPECTED_FRAUD';

    /**
     * Retrieve custom order states.
     *
     * @return array
     */
    public static function getAll()
    {
        return array(
            self::PENDING => array(
                'translations' => array(
                    'sv' => 'Väntar på Resurs Bank',
                    'en' => 'Waiting for Resurs Bank'
                ),
                'settings' => array(
                    'color' => '#BBBB00',
                    'send_email' => 0
                )
            ),
            self::PROCESSING => array(
                'translations' => array(
                    'sv' => 'Bekräftad av Resurs Bank',
                    'en' => 'Confirmed by Resurs Bank'
                ),
                'settings' => array(
                    'paid' => 1,
                    'send_email' => 1,
                    'template' => 'payment',
                    'color' => '#008800'
                )
            ),
            self::COMPLETED => array(
                'translations' => array(
                    'sv' => 'Färdig hos Resurs Bank',
                    'en' => 'Completed at Resurs Bank',
                ),
                'settings' => array(
                    'paid' => 1,
                    'color' => '#32FF32',
                    'send_email' => 0
                )
            ),
            self::SUSPECTED_FRAUD => array(
                'translations' => array(
                    'sv' => 'Granskas av Resurs Bank',
                    'en' => 'Being reviewed by Resurs Bank',
                ),
                'settings' => array(
                    'color' => '#008800',
                    'send_email' => 0
                )
            )
        );
    }

    /**
     * Create an order state.
     *
     * @param ResursBankCheckout $module
     * @param array $data
     * @return int
     * @throws Exception
     */
    public static function create(
        ResursBankCheckout $module,
        array $data
    ) {
        if (!isset($data['translations']) || !isset($data['settings'])) {
            throw new Exception('Improper state data provided.');
        }

        // Create order state.
        $state = new PrestaOrderState($data);

        // Add some static values to our state data.
        $state->module_name = $module->getName();
        $state->unremovable = 1;
        $state->logable = 1;
        $state->name = self::compileTranslationsArray($data['translations']);
        $state->template = '';

        foreach ($data['settings'] as $key => $val) {
            $state->{$key} = $val;
        }

        $state->add();

        if (!isset($state->id) || (int) $state->id === 0) {
            throw new Exception('Failed to create order state using data ' . @json_encode($data));
        }

        return $state;
    }

    /**
     * Compile an array with order state translation for each available language.
     *
     * @param array $data
     * @return array
     * @throws Exception
     */
    private static function compileTranslationsArray(array $data)
    {
        $result = array();

        // Insert locale translations for order state.
        $languages = Language::getLanguages();

        if (count($languages)) {
            foreach ($languages as $language) {
                if (!is_array($language) || !isset($language['id_lang'])) {
                    throw new Exception('Improperly formatted language data.');
                }

                if (isset($data[$language['iso_code']])) {
                    $result[$language['id_lang']] = $data[$language['iso_code']];
                } else if (isset($data['en'])) {
                    $result[$language['id_lang']] = $data['en'];
                }
            }
        }

        return $result;
    }

    /**
     * Install custom order states.
     *
     * @param ResursBankCheckout $module
     */
    public static function install(ResursBankCheckout $module)
    {
        if (!self::isInstalled($module)) {
            foreach (self::getAll() as $code => $data) {
                // Only create an order state if it doesn't already exist.
                if ((bool) Configuration::get($code) === false) {
                    /** @var PrestaOrderState $state */
                    $state = self::create($module, $data);

                    // Store in config which id belongs to the state code (so we can later retrieve it easily).
                    Configuration::updateValue($code, $state->id);

                    // Install logotype.
                    self::installLogotype($module, $state);
                }
            }
        }
    }

    /**
     * Install order state logotype.
     *
     * @param ResursBankCheckout $module
     * @param PrestaOrderState $state
     * @throws Exception
     */
    private static function installLogotype(
        ResursBankCheckout $module,
        PrestaOrderState $state
    ) {
        if (!isset($state->id) || (int) $state->id === 0) {
            throw new Exception('Cannot install logotype for order state without an id.');
        }

        // Get source of our logotype (using the same logotype for all order states atm).
        $source = $module->getPath('/views/img/orderstatus_logo.gif', true);

        if (!file_exists($source)) {
            throw new Exception('Missing order state logotype source ' . $source);
        }

        // Get logotype destination directory.
        $destinationDirectory = (string) realpath($module->getPath('/../../img/os/', true));

        if (!is_dir($destinationDirectory)) {
            throw new Exception('Order state logotype destination directory does not exist ' . $destinationDirectory);
        }

        // Get complete file destination path.
        $destination = ($destinationDirectory . '/' . (int) $state->id . '.gif');

        // Copy the logotype.
        @copy($source, $destination);

        // Make sure everything went fine.
        if (!file_exists($destination)) {
            throw new Exception('Failed to copy order state logotype for ' . (int) $state->id);
        }
    }

    /**
     * Check if custom order states are installed.
     *
     * @param ResursBankCheckout $module
     * @return bool
     */
    public static function isInstalled(ResursBankCheckout $module)
    {
        $orderStates = Db::getInstance()->executeS(
            "SELECT id_order_state FROM " .
            _DB_PREFIX_ . "order_state " .
            "WHERE module_name = '" . $module->getName() . "'"
        );

        return count($orderStates) > 0;
    }

    /**
     * Uninstall order states.
     *
     * @param ResursBankCheckout $module
     */
    public static function uninstall(ResursBankCheckout $module)
    {
        if (self::isInstalled($module)) {
            foreach (self::getAll() as $code => $data) {
                $stateId = Configuration::get($code);

                // Only create an order state if it doesn't already exist.
                if ((bool) $stateId !== false) {
                    /** @var PrestaOrderState $state */
                    $state = new PrestaOrderState($stateId);
                    $state->delete();

                    // Delete states from the database.
                    Configuration::deleteByName($code);

                    // Delete logotype.
                    self::deleteLogotype($module, $state);
                }
            }
        }
    }

    /**
     * Delete order state logotype.
     *
     * @param ResursBankCheckout $module
     * @param PrestaOrderState $state
     * @throws Exception
     */
    private static function deleteLogotype(
        ResursBankCheckout $module,
        PrestaOrderState $state
    ) {
        if (!isset($state->id) || (int) $state->id === 0) {
            throw new Exception('Cannot delete logotype for order state without an id.');
        }

        // Get source of our logotype (using the same logotype for all order states atm).
        $source = (string) realpath($module->getPath('/../../img/os/'. (int) $state->id . '.gif', true));

        if (file_exists($source)) {
            // Delete the logotype.
            @unlink($source);

            // Make sure everything went fine.
            if (file_exists($source)) {
                throw new Exception('Failed to delete order state logotype for ' . (int) $state->id);
            }
        }
    }
}
