<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace ResursBankCheckout\Models\Hooks;

use \ResursBankCheckout;
use \Exception;
use \Country;
use \ResursBankCheckout\Models\Service\Config;
use \Configuration;
use \ResursBankCheckout\Models\Service\Message\Checkout;

/**
 * Class Footer
 *
 * Hook for the event "Footer". Injects JavaScript code on the checkout page to display the iframe (injected through the
 * hook "Header") and initiate the JavaScript code necessary to communicate with the iframe and backend.
 *
 * @package ResursBankCheckout\Models\Hooks
 */
class Footer
{
    /**
     * @var ResursBankCheckout
     */
    private $module;

    /**
     * Event parameters.
     *
     * @var array
     */
    private $params;

    /**
     * Footer constructor.
     *
     * @param ResursBankCheckout $module
     * @param array $params
     */
    public function __construct(
        ResursBankCheckout $module,
        array $params = array()
    ) {
        $this->module = $module;
        $this->params = $params;
    }

    /**
     * Display
     *
     * @return string
     * @throws Exception
     */
    public function execute()
    {
        $result = '';

        try {
            if ($this->isActive()) {
                $this->module->getContext()->smarty->assign(array(
                    'baseUrl' => $this->module->getBaseUrl(),
                    'backUrl' => $this->module->getApi()->getBackUrl(),
                    'iframeUrl' => $this->module->getApi()->getIframeUrl(),
                    'iframe' => $this->getIframeHtml(),
                    'saveOrderUrl' => $this->getSaveOrderUrl(),
                    'saveBillingUrl' => $this->getSaveBillingUrl(),
                    'saveShippingUrl' => $this->getSaveShippingUrl(),
                    'savePaymentUrl' => $this->getSavePaymentUrl(),
                    'storeSsn' => Config::getStoreSsn(),
                    'errorMessages' => $this->getErrorMessages()
                ));

                $result = $this->module->renderView('hook/js.tpl');
            }
        } catch (Exception $e) {
             $this->module->log($e);
        }

        return $result;
    }

    /**
     * Retrieve iframe HTML.
     *
     * @return string
     * @throws Exception
     */
    private function getIframeHtml()
    {
        $result = '';

        try {
            $result = $this->module->getApi()->getIframe();
        } catch (Exception $e) {
            $this->module->log($e);

            // Display generic error message.
            Checkout::add(
                Checkout::TYPE_ERROR,
                $this->module->l('There was an error communicating with the Resurs Bank payment gateway. Please refresh the page. If the problem persists please contact our support.')
            );
        }

        return $result;
    }

    /**
     * Retrieve URL to saveorder controller.
     *
     * @return string
     */
    private function getSaveOrderUrl()
    {
        return (string) $this->module->getSimpleLink('saveorder');
    }

    /**
     * Retrieve URL to savebilling controller (saves entered billing address).
     *
     * @return string
     */
    private function getSaveBillingUrl()
    {
        return (string) $this->module->getSimpleLink('savebilling');
    }

    /**
     * Retrieve URL to saveshipping controller (saves entered shipping address).
     *
     * @return string
     */
    private function getSaveShippingUrl()
    {
        return (string) $this->module->getSimpleLink('saveshipping');
    }

    /**
     * Retrieve URL to savepayment controller.
     *
     * @return string
     */
    private function getSavePaymentUrl()
    {
        return (string) $this->module->getSimpleLink('savepayment');
    }

    /**
     * Check whether or not this hook is active.
     *
     * @return bool
     */
    public function isActive()
    {
        return (
            $this->module->enabled() &&
            $this->module->isOnCheckoutPage() &&
            $this->module->cartExistsInContext()
        );
    }

    /**
     * Retrieve contents of error message bag.
     *
     * @return array
     */
    public function getErrorMessages()
    {
        return Checkout::get(Checkout::TYPE_ERROR);
    }
}
