<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace ResursBankCheckout\Models\Hooks;

use \ResursBankCheckout;
use \Exception;
use \DispatcherCore;
use \Hook;

/**
 * Class ActionFrontControllerSetMedia
 *
 * Hook for event "ActionFrontControllerSetMedia". This is useful when hooking otherwise inaccessible processes, like
 * applying a discount code. Please note we use this instead of "ActionDispatcher" because we want the cart to exist
 * within our context, and the currently called action to have been processed (for example, when applying a discount
 * coupon we want the coupon to have been applied before we execute this hook).
 *
 * @package ResursBankCheckout\Models\Hooks
 */
class ActionFrontControllerSetMedia
{
    /**
     * @var ResursBankCheckout
     */
    private $module;

    /**
     * Event parameters.
     *
     * @var array
     */
    private $params;

    /**
     * ActionFrontControllerSetMedia constructor.
     *
     * @param ResursBankCheckout $module
     * @param array $params
     */
    public function __construct(
        ResursBankCheckout $module,
        array $params = array()
    ) {
        $this->module = $module;
        $this->params = $params;
    }

    /**
     * Update payment at Resurs Bank to match current cart information. The
     * "actionSaveCart" hook will be executed whenever an action stemming from
     * the "cart" or "orderopc" controllers are executed. This may seem
     * excessive, but due to the poor design pattern applied in Prestashop we
     * have no other way of doing this cleanly and securely.
     */
    public function execute()
    {
        try {
            if ($this->isActive()) {
                // Please see the docblock for an explanation of why we perform this so often.
                if ($this->isCheckoutPageAction()) {
                    Hook::exec('actionCartSave');
                }
            }
        } catch (Exception $e) {
            $this->module->log($e, $_REQUEST);
        }
    }

    /**
     * Check if this hook is activate.
     *
     * @return bool
     */
    public function isActive()
    {
        return $this->module->enabled();
    }

    /**
     * Check if we the action being executed is stemming from the checkout page.
     *
     * @return bool
     */
    public function isCheckoutPageAction()
    {
        $currentController = DispatcherCore::getInstance()->getController();

        return (
            ($currentController === "orderopc" || $currentController === "cart")
        );
    }
}
