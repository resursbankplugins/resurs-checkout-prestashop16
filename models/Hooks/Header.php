<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace ResursBankCheckout\Models\Hooks;

use \ResursBankCheckout;
use \Exception;
use \Country;

/**
 * Class Header
 *
 * Hook for the event "Header". This will inject the iframe on the checkout page. We do not include this in the "Footer"
 * hook observer because we want the iframe to start loading ASAP. We keep it hidden until the page has finished loading
 * and then move it to it's correct location and display it using JavaScript code included in the footer.
 *
 * @package ResursBankCheckout\Models\Hooks
 */
class Header
{
    /**
     * @var ResursBankCheckout
     */
    private $module;

    /**
     * Event parameters.
     *
     * @var array
     */
    private $params;

    /**
     * Header constructor.
     *
     * @param ResursBankCheckout $module
     * @param array $params
     */
    public function __construct(
        ResursBankCheckout $module,
        array $params = array()
    ) {
        $this->module = $module;
        $this->params = $params;
    }

    /**
     * Load and display iframe on checkout page.
     *
     * @return string
     */
    public function execute()
    {
        $result = '';

        try {
            if ($this->isActive()) {
                // Add CSS.
                $this->module->addCSS('views/css/front.css');

                // Assign template variables.
                $this->module->getContext()->smarty->assign(array(
                    '_version' => $this->module->getVersion(),
                    '_path' => $this->module->getPath(),
                    '_defaultCountry' => Country::getDefaultCountryId(),
                    '_RESURSCHECKOUT_IFRAME_URL' => $this->module->getApi()->getUrl()
                ));

                // Render checkout page.
                $result = $this->renderIframe();
            }
        } catch (Exception $e) {
            // Log error.
            $this->module->log($e);
        }

        return $result;
    }

    /**
     * Render the iframe element on the checkout page.
     *
     * @return string
     * @throws Exception
     */
    public function renderIframe()
    {
        $result = '';

        try {
            $this->module->addJS('global/resursbank.js')
                ->addJS('global/classy/classy.js')
                ->addJS('front/checkout.js')
                ->addJS('front/book-order.js')
                ->addJS('front/payment-method.js')
                ->addJS('front/user-address.js')
                ->addJS('front/error-handler.js')
                ->addJS('front/message/message.js')
                ->addJS('front/message/error-message.js')
                ->addJS('front/ajax-q/ajax-q.js')
                ->addJS('front/ajax-q/ajax-q-chain.js')
                ->addJS('front/ajax-loader/ajax-overlay.js')
                ->addJS('front/ajax-loader/ajax-loader.js');
        } catch (Exception $e) {
            $this->module->log($e);
            $result = $this->module->l('There was an error rendering the checkout page.');
        }

        return $result;
    }

    /**
     * Check whether or not this hook is active.
     *
     * @return bool
     */
    public function isActive()
    {
        return (
            $this->module->enabled() &&
            $this->module->isOnCheckoutPage() &&
            $this->module->cartExistsInContext()
        );
    }
}
