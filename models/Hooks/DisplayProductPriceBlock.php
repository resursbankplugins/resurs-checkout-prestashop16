<?php

// displayProductContent

/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace ResursBankCheckout\Models\Hooks;

use \ResursBankCheckout;
use \Exception;

class DisplayProductPriceBlock
{
    /**
     * @var ResursBankCheckout
     */
    private $module;

    /**
     * Event parameters.
     *
     * @var array
     */
    private $params;

    /**
     * ActionCartSave constructor.
     *
     * @param ResursBankCheckout $module
     * @param array $params
     */
    public function __construct(
        ResursBankCheckout $module,
        array $params = array()
    ) {
        $this->module = $module;
        $this->params = $params;
    }

    /**
     * Pick the correct placement of annuitys and send it to screen, with start here.
     *
     * @return float|string
     * @throws Exception
     */
    public function execute()
    {
        $result = '';
        try {
            if ($this->isActive() && $this->params['type'] === 'after_price') {
                $result = $this->getAnnuityHtml();
            }
        } catch (Exception $e) {
            $this->module->log($e, $_REQUEST);
        }

        return $result;
    }

    /**
     * Convert array to ecom-compliant data.
     *
     * @param $array
     * @return object
     */
    private function getArrayAsObject($array)
    {
        return (object)$array;
    }

    /**
     * Get annuity factor price on fly with help from ECom.
     *
     * @return float
     * @throws Exception
     */
    public function getAnnuityPriceByConfiguredDuration()
    {
        $currentMethod = $this->getSelectedMethod();
        $choiceOfDuration = \Configuration::get(ResursBankCheckout\Models\Service\Config::SETTING_PARTPAYMENT_DURATION);
        $annuityList = \ResursBankCheckout\Models\Service\Payment::getAnnuity();

        if (isset($annuityList[$currentMethod])) {
            try {
                $annuityDurations = array_map(
                    array($this, 'getArrayAsObject'),
                    $annuityList[$currentMethod]
                );

                $result = $this->ecom()->getAnnuityPriceByDuration(
                    $this->params['product']->getPrice(),
                    $annuityDurations,
                    $choiceOfDuration
                );

            } catch (\Exception $e) {
                $result = $e->getMessage();
            }
        }

        return $result;
    }

    /**
     * Get annuity factor string from a rendered template.
     *
     * @return string
     * @throws Exception
     */
    public function getAnnuityHtml()
    {
        // Price alone
        $annuityPrice = $this->getAnnuityPriceByConfiguredDuration();
        $currency = \Context::getContext()->currency->getSign();

        // Assign template variables.
        $this->module->getContext()->smarty->assign(array(
                'annuityPrice' => $annuityPrice,
                'currency' => $currency,
                'readMoreUrl' => $this->module->getSimpleLink(
                    'costofpurchase',
                    array(
                        'paymentmethod' => $this->getSelectedMethod(),
                        'amount' => $this->params['product']->getPrice()
                    )
                )
            )
        );

        // Render the template.
        $result = $this->module->display(
            $this->module->getPath('resursbankcheckout.php'),
            'product_annuity.tpl'
        );

        return $result;
    }

    /**
     * Generic simplifier to get the configured payment method to use with annuitys.
     *
     * @return string
     */
    private function getSelectedMethod()
    {
        return \Configuration::get(ResursBankCheckout\Models\Service\Config::SETTING_PARTPAYMENT_METHOD);
    }

    /**
     * Returns true if merchant has chosen a payment method that supports annuity factors.
     *
     * @return bool
     * @throws Exception
     */
    public function isActive()
    {
        $return = false;
        $context = \Context::getContext();
        $controller = $context->controller;
        $controllerClass = get_class($controller);
        $selectedMethod = $this->getSelectedMethod();
        if ($controllerClass === 'ProductController' && !empty($selectedMethod) &&
            ($selectedMethod !== 'disabled')
        ) {
            $return = true;
        }

        return $return;
    }

    /**
     * Retrieve ECom library instance.
     *
     * @return \Resursbank\RBEcomPHP\ResursBank
     * @throws Exception
     */
    protected function ecom()
    {
        return $this->module->getApi()->ecom();
    }
}
