<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace ResursBankCheckout\Models\Hooks;

use \Tools;
use \ResursBankCheckout;
use \Exception;

/**
 * Class BackOfficeHeader
 *
 * Hook for the event "BackOfficeHeader". This will include administration panel specific JavaScript files.
 *
 * @package ResursBankCheckout\Models\Hooks
 */
class BackOfficeHeader
{
    /**
     * @var ResursBankCheckout
     */
    private $module;

    /**
     * Event parameters.
     *
     * @var array
     */
    private $params;

    /**
     * BackOfficeHeader constructor.
     *
     * @param ResursBankCheckout $module
     * @param array $params
     */
    public function __construct(
        ResursBankCheckout $module,
        array $params = array()
    ) {
        $this->module = $module;
        $this->params = $params;
    }

    /**
     * Include administration panel specific JavaScript files.
     *
     * @return string
     */
    public function execute()
    {
        $result = '';

        try {
            if ($this->isActive() || (bool)Tools::getValue('RESURSBANKCHECKOUT_LIVE_MODE')) {
                $result = "
                    \n<script>var RESURSBANK_BASEURL = '{$this->module->getBaseUrl()}';</script>
                ";

                if (Tools::getValue('module_name') === $this->module->getName()) {
                    $this->module->addJS('global/resursbank.js')
                        ->addJS('global/classy/classy.js')
                        ->addJS('admin/checkout.js');
                }

                $this->module->addJS('admin/payment_information.js')
                    ->addCSS('views/css/back.css');
            }
        } catch (Exception $e) {
            $this->module->log($e);
        }

        return $result;
    }

    /**
     * Check whether or not this hook is active.
     *
     * @return bool
     */
    public function isActive()
    {
        return $this->module->enabled();
    }
}
