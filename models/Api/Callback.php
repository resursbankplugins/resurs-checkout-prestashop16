<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace ResursBankCheckout\Models\Api;

use \Exception;
use \Configuration;
use \ResursBankCheckout\Models\Api as ApiModel;
use \ResursBankCheckout\Models\Service\Config;

/**
 * Class Callback
 *
 * Contains Api
 *
 * @package ResursBankCheckout\Models\Api
 */
class Callback
{
    /**
     * @var ApiModel
     */
    private $api;

    /**
     * Callback API handler constructor.
     *
     * @param ApiModel $api
     */
    public function __construct(ApiModel $api)
    {
        $this->api = $api;
    }

    /**
     * Register callbacks.
     *
     * @return $this
     * @throws Exception
     */
    public function registerCallbacks()
    {
        // Generate a new SALT.
        $this->generateSalt();

        // Register callback URLs.
        foreach ($this->getCallbackRegistrationEvents() as $event) {
            $this->registerCallback($event);
        }

        //\Hook::exec('resursRegisterCallback', []);

        return $this;
    }

    /**
     * Generate new SALT key.
     *
     * @return $this
     * @throws Exception
     */
    private function generateSalt()
    {
        // Generate a new callback SALT key.
        $salt = $this->api->ecom()->getSaltKey(4, 10);

        // Register callback URLs.
        $this->api->ecom()->setCallbackDigest($salt);

        // Store generated SALT in database.
        Configuration::updateValue(
            Config::SETTING_SALT,
            $salt
        );

        return $this;
    }

    /**
     * Retrieve a list of all events to be registered as callbacks.
     *
     * @return array
     */
    public function getCallbackRegistrationEvents()
    {
        return array(
            'annulment',
            'automatic_fraud_control',
            'booked',
            'finalization',
            'unfreeze',
            'update',
            'test'
        );
    }

    /**
     * Register callback URL for an event (such as "unfreeze" or "booked").
     *
     * @param string $event
     * @return $this
     * @throws Exception
     */
    public function registerCallback($event)
    {
        $this->api->ecom()->setRegisterCallback(
            $this->getCallbackId($event),
            $this->getCallbackUrl($event)
        );

        return $this;
    }

    /**
     * Retrieve callback URL based on event name.
     *
     * @param string $event
     * @return string
     */
    public function getCallbackUrl($event)
    {
        // Callback URL to be registered.
        $result = $this->api->getModule()->getContext()->link->protocol_content .
            $this->api->getModule()->getContext()->shop->domain .
            $this->api->getModule()->getContext()->shop->physical_uri .
            'index.php?fc=module';

        // ADd callback parameters to URL (first one is included already since it's static and helps us avoid a regex).
        foreach ($this->getCallbackParameters($event) as $key => $val) {
            $result.= '&' . $key . '=' . $val;
        }

        return $result;
    }

    /**
     * Retrieve value of callback ID constant.
     *
     * @param string $event
     * @return int
     * @throws Exception
     */
    public function getCallbackId($event)
    {
        $callbackIdConstant = '\Resursbank\RBEcomPHP\RESURS_CALLBACK_TYPES::CALLBACK_TYPE_' . strtoupper($event);

        if (!defined($callbackIdConstant)) {
            throw new Exception(
                'Constant \Resursbank\RBEcomPHP\RESURS_CALLBACK_TYPES::CALLBACK_TYPE_' . strtoupper($event) .
                ' is undefined. Unable to register callback for ' . $event
            );
        }

        return (int) constant($callbackIdConstant);
    }

    /**
     * Get callback URL parameters by event.
     *
     * @param string $event
     * @return array
     */
    public function getCallbackParameters($event)
    {
        // Basic parameters which always need to be included in the callback URL.
        $result = array(
            'module' => $this->api->getModule()->getName(),
            'controller' => 'callback',
            'event' => $event
        );

        // "test" parameters differ from all other callback URLs.
        if ($event === 'test') {
            $result = array_merge($result, array(
                'prm1' => 'param1',
                'prm2' => 'param2',
                'prm3' => 'param3',
                'prm4' => 'param4',
                'prm5' => 'param5'
            ));
        } else {
            $result = array_merge($result, array(
                'paymentId' => '{paymentId}',
                'digest' => '{digest}',
                't' => time()
            ));
        }

        /**
         * We need an additional "result" parameter (which will have the value "FROZEN" or "THAWED") for
         * the automatic_fraud_control callback.
         */
        if ($event === 'automatic_fraud_control') {
            $result['result'] = '{result}';
        }

        return $result;
    }

    /**
     * Retrieve registered callbacks.
     *
     * @return array
     * @throws Exception
     */
    public function getRegisteredCallbacks()
    {
        $result = array();

        // Validate user credentials first.
        if ($this->api->validateCredentials()) {
            // Fetch registered callbacks.
            $result = $this->api->ecom()->getCallBacksByRest(true);

            // Validate result.
            if (!is_array($result)) {
                $result = array();
            }

            // Sort result.
            ksort($result);
        }

        return $result;
    }

    /**
     * Retrieve and sort registered callbacks.
     *
     * @return array
     * @throws Exception
     */
    public function getSortedCallbacks()
    {
        $result = $this->api->ecom()->getCallBacksByRest(true);

        if (!is_array($result)) {
            throw new Exception('Retrieved callbacks should be an array.');
        }

        ksort($result);

        return $result;
    }
}
