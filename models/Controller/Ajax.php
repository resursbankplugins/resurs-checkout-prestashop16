<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace ResursBankCheckout\Models\Controller;

use \ResursBankCheckout;

/**
 * Class Ajax
 *
 * Common methods to handle AJAX based controllers.
 *
 * @package ResursBankCheckout\Models\Controller
 */
class Ajax
{
    /**
     * @var array
     */
    protected $errors;

    /**
     * @var ResursBankCheckout
     */
    private $module;

    /**
     * @var mixed
     */
    private $controller;

    /**
     * @var array
     */
    private $responseData;

    /**
     * General constructor.
     *
     * @param ResursBankCheckout $module
     * @param mixed $controller
     */
    public function __construct(ResursBankCheckout $module, $controller)
    {
        $this->module = $module;
        $this->controller = $controller;
        $this->errors = array();
        $this->responseData = array();
    }

    /**
     * Respond to a request.
     */
    public function respond()
    {
        $result = array(
            'data' => $this->responseData,
            'message' => array(
                'error' => $this->errors
            )
        );

        header('Content-Type: application/json', true, 200);

        die(json_encode($result));
    }

    /**
     * Add response data.
     *
     * @param string $key
     * @param mixed $data
     * @return $this
     */
    public function addResponseData($key, $data)
    {
        $this->responseData[(string) $key] = $data;

        return $this;
    }

    /**
     * Add error message.
     *
     * @param string $message
     * @return $this
     */
    public function addError($message)
    {
        $this->errors[] = (string) $message;

        return $this;
    }

    /**
     * @return ResursBankCheckout
     */
    public function getModule()
    {
        return $this->module;
    }
}
