<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once(dirname(__FILE__) . '/vendor/autoload.php');
require_once(dirname(__FILE__) . '/models/Hooks/DisplayBackOfficeTop.php');
require_once(dirname(__FILE__) . '/models/Hooks/BackOfficeHeader.php');
require_once(dirname(__FILE__) . '/models/Hooks/Header.php');
require_once(dirname(__FILE__) . '/models/Hooks/ActionCartSave.php');
require_once(dirname(__FILE__) . '/models/Hooks/ActionFrontControllerSetMedia.php');
require_once(dirname(__FILE__) . '/models/Hooks/Footer.php');
require_once(dirname(__FILE__) . '/models/Hooks/DisplayProductPriceBlock.php');
require_once(dirname(__FILE__) . '/models/Items.php');
require_once(dirname(__FILE__) . '/models/Items/Cart.php');
require_once(dirname(__FILE__) . '/models/Items/Order.php');
require_once(dirname(__FILE__) . '/models/Api.php');
require_once(dirname(__FILE__) . '/models/Api/Callback.php');
require_once(dirname(__FILE__) . '/models/Service/Config.php');
require_once(dirname(__FILE__) . '/models/Service/Setup.php');
require_once(dirname(__FILE__) . '/models/Controller/Ajax.php');
require_once(dirname(__FILE__) . '/models/Service/Session.php');
require_once(dirname(__FILE__) . '/models/Service/Cart.php');
require_once(dirname(__FILE__) . '/models/Service/Order.php');
require_once(dirname(__FILE__) . '/models/Service/Order/State.php');
require_once(dirname(__FILE__) . '/models/Service/Customer.php');
require_once(dirname(__FILE__) . '/models/Service/Customer/Address.php');
require_once(dirname(__FILE__) . '/models/Service/Customer/Address/Billing.php');
require_once(dirname(__FILE__) . '/models/Service/Customer/Address/Shipping.php');
require_once(dirname(__FILE__) . '/models/Service/Customer/Address/Validate.php');
require_once(dirname(__FILE__) . '/models/Service/Payment.php');
require_once(dirname(__FILE__) . '/models/Service/Customer/Validate.php');
require_once(dirname(__FILE__) . '/models/Service/Message.php');
require_once(dirname(__FILE__) . '/models/Service/Message/Checkout.php');
require_once(dirname(__FILE__) . '/models/Callback.php');
require_once(dirname(__FILE__) . '/models/Log.php');
require_once(dirname(__FILE__) . '/models/Log/Api.php');
require_once(dirname(__FILE__) . '/models/Log/Callback.php');
require_once(dirname(__FILE__) . '/models/Exception/MissingDataException.php');

/**
 * Class ResursBankCheckout
 *
 * Contains general methods for the module.
 */
class ResursBankCheckout extends PaymentModule
{
    /**
     * Module name.
     */
    const NAME = 'resursbankcheckout';

    /**
     * Module version.
     */
    const VERSION = '2.6.11';

    /**
     * Module author name.
     */
    const AUTHOR = 'Resurs Bank';

    /**
     * @var \ResursBankCheckout\Models\Api
     */
    private $api;

    /**
     * @var \ResursBankCheckout\Models\Controller\Ajax
     */
    private $ajaxController;

    /**
     * @var \ResursBankCheckout\Models\Log
     */
    private $logHandler;

    /**
     * ResursBankCheckout constructor.
     */
    public function __construct()
    {
        $this->startSession();

        // Initial module specification.
        $this->name = self::NAME;
        $this->tab = \ResursBankCheckout\Models\Service\Config::TAB;
        $this->version = self::VERSION;
        $this->author = self::AUTHOR;

        // Whether or not to create an instance of tis class when in backoffice (no idea why).
        $this->need_instance = 1;

        // Whether or not to load bootstrap CSS.
        $this->bootstrap = true;

        parent::__construct();

        // Conclude module specification.
        $this->displayName = $this->l('Resurs Bank Checkout');
        $this->description = $this->l('Payment gateway for Resurs Bank Checkout');
        $this->confirmUninstall = $this->l('Are you sure?');
        $this->ps_versions_compliancy = [
            'min' => '1.6',
            'max' => _PS_VERSION_,
        ];
    }

    /**
     * Start session.
     *
     * @return $this
     */
    private function startSession()
    {
        \ResursBankCheckout\Models\Service\Session::start();

        return $this;
    }

    /**
     * Run installation procedure.
     *
     * @return bool
     * @throws Exception
     */
    public function install()
    {
        $result = true;

        try {
            // Run parent installation procedure.
            if (!(bool)parent::install()) {
                throw new Exception('Parent installation procedure failed.');
            }

            // Install module.
            \ResursBankCheckout\Models\Service\Setup::install($this);
        } catch (Exception $e) {
            $this->log($e);

            $result = false;
        }

        return $result;
    }

    /**
     * Run uninstallation procedure.
     *
     * @return bool
     * @throws Exception
     */
    public function uninstall()
    {
        $result = true;

        try {
            // Uninstall module.
            \ResursBankCheckout\Models\Service\Setup::uninstall($this);

            // Run parent installation procedure.
            if (!(bool)parent::uninstall()) {
                throw new Exception('Parent uninstallation procedure failed.');
            }
        } catch (Exception $e) {
            $this->log($e);

            $result = false;
        }

        return $result;
    }

    /**
     * Retrieve Ajax controller helper.
     *
     * @param mixed $controller
     * @return \ResursBankCheckout\Models\Controller\Ajax
     */
    public function getAjaxController($controller)
    {
        if ($this->ajaxController === null) {
            $this->ajaxController = new \ResursBankCheckout\Models\Controller\Ajax($this, $controller);
        }

        return $this->ajaxController;
    }

    /**
     * Retrieve API model.
     *
     * @return \ResursBankCheckout\Models\Api
     */
    public function getApi()
    {
        if ($this->api === null) {
            $this->api = new \ResursBankCheckout\Models\Api($this);
        }

        return $this->api;
    }

    /**
     * Retrieve context.
     *
     * @return ContextCore
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * Get module name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Retrieve module version.
     *
     * @return float
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Retrieve module id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Return path to the module directory. This is used by both URL:s (relative path) and filesystem operations
     * (absolute path).
     *
     * @param string $path Path within module directory.
     * @param bool $absolute
     * @return string
     */
    public function getPath($path = '', $absolute = false)
    {
        $path = !is_string($path) ? '' : $path;

        return $this->getBasePath($absolute) . $path;
    }

    /**
     * Retrieve module base path (this is used by both URL:s (relative path) and filesystem operations (absolute path).
     *
     * @param bool $absolute
     * @return string
     */
    public function getBasePath($absolute = false)
    {
        return !$absolute ? (string)$this->_path : dirname(__FILE__);
    }

    /**
     * Check whether or not we are on the checkout page.
     *
     * @return bool
     * @throws PrestaShopException
     */
    public function isOnCheckoutPage()
    {
        return DispatcherCore::getInstance()->getController() === 'orderopc';
    }

    /**
     * Add JavaScript source to header.
     *
     * @param string $path
     * @return $this
     */
    public function addJS($path)
    {
        $this->getContext()->controller->addJS(
            $this->getPath('views/js/' . $path)
        );

        return $this;
    }

    /**
     * Add CSS source to header.
     *
     * @param string $path
     * @return $this
     */
    public function addCSS($path)
    {
        $this->getContext()->controller->addCSS($this->getPath($path));

        return $this;
    }

    /**
     * Retrieve base URL.
     *
     * @param bool $includePhysicalUri
     * @return string
     */
    public function getBaseUrl($includePhysicalUri = false)
    {
        return $this->getContext()->link->protocol_content .
            $this->getContext()->shop->domain .
            ($includePhysicalUri ?
                $this->getContext()->shop->physical_uri :
                ''
            );
    }

    /**
     * Render module configuration page.
     *
     * @return string
     * @throws Exception
     */
    public function getContent()
    {
        $config = new \ResursBankCheckout\Models\Service\Config($this);

        if ($config->submitted() && $config->validates()) {
            $config->save();
        }

        return $config->render();
    }

    /**
     * Render view.
     *
     * @param string $path
     * @return string
     * @throws Exception
     */
    public function renderView($path)
    {
        $result = '';

        try {
            $result = $this->context->smarty->fetch($this->local_path . 'views/templates/' . $path);

            if (!is_string($result)) {
                throw new Exception('Failed to render template with path ' . $path);
            }
        } catch (Exception $e) {
            $this->log($e, ['path' => $path]);

            $result = $this->l('Failed to render Resurs Bank template.');
        }

        return $result;
    }

    /**
     * Hook to display additional Resurs Bank information on order view.
     *
     * @param array $params
     * @return string
     */
    public function hookDisplayBackOfficeTop(array $params = [])
    {
        $hook = new \ResursBankCheckout\Models\Hooks\DisplayBackOfficeTop($this, $params);

        return $hook->execute();
    }

    /**
     * Add custom JavaScript and CSS sources to admin (back office) header section.
     *
     * @param array $params
     * @return string
     * @throws Exception
     */
    public function hookBackOfficeHeader(array $params = [])
    {
        $hook = new \ResursBankCheckout\Models\Hooks\BackOfficeHeader($this, $params);

        return $hook->execute();
    }

    /**
     * Display iframe in checkout and include JavaScript / CSS files in header.
     *
     * @param array $params
     * @return string
     */
    public function hookHeader(array $params = [])
    {
        $hook = new \ResursBankCheckout\Models\Hooks\Header($this, $params);

        return $hook->execute();
    }

    /**
     * Executes when cart contents changes.
     *
     * @param array $params
     * @return string
     */
    public function hookActionCartSave(array $params = [])
    {
        $hook = new \ResursBankCheckout\Models\Hooks\ActionCartSave($this, $params);
        $hook->execute();
    }

    /**
     * Executes when controller action is executed.
     *
     * @param array $params
     * @return string
     */
    public function hookActionFrontControllerSetMedia(array $params = [])
    {
        $hook = new \ResursBankCheckout\Models\Hooks\ActionFrontControllerSetMedia($this, $params);
        $hook->execute();
    }

    /**
     * Inject JavaScript initialization before body end.
     *
     * @param array $params
     * @return string
     * @throws Exception
     */
    public function hookFooter(array $params = [])
    {
        $hook = new \ResursBankCheckout\Models\Hooks\Footer($this, $params);

        return $hook->execute();
    }

    public function hookDisplayProductPriceBlock(array $params = [])
    {
        $hook = new \ResursBankCheckout\Models\Hooks\DisplayProductPriceBlock($this, $params);

        return $hook->execute();
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function hookUpdateResursSsn(array $params = array()) {
    }

    /**
     * Check if module is enabled.
     *
     * @return bool
     */
    public function enabled()
    {
        return (
            parent::isEnabled($this->name) &&
            (int)Configuration::get(\ResursBankCheckout\Models\Service\Config::SETTING_ACTIVE) === 1
        );
    }

    /**
     * Retrieve completely random string.
     *
     * @param int $length
     * @param string $charset
     * @return string
     */
    public function strRand($length, $charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')
    {
        $result = '';

        $length = (int)$length;

        if ($length > 0) {
            $max = strlen($charset) - 1;

            for ($i = 0; $i < $length; $i++) {
                $result .= $charset[mt_rand(0, $max)];
            }
        }

        return $result;
    }

    /**
     * Construct and return module controller URL.
     *
     * @param string $controller
     * @return string
     */
    public function getModuleUrl($controller)
    {
        return (string)$this->getContext()->link->getModuleLink(
            'resursbankcheckout',
            (string)$controller,
            [],
            true
        );
    }

    /**
     * The cart will appear in the context during controller initialization, however hooks (like ActionCartSave, which
     * expects the cart to be within the context) will execute prior to controller initialization. Therefore, we need
     * some very strict checks to ensure the cart exists within the context before we attempt to use it.
     *
     * @return bool
     */
    public function cartExistsInContext()
    {
        return (
            isset($this->getContext()->cart) &&
            $this->getContext()->cart !== null &&
            ($this->getContext()->cart instanceof Cart) &&
            isset($this->getContext()->cart->id) &&
            (int)$this->getContext()->cart->id !== 0
        );
    }

    /**
     * Create log entry.
     *
     * @param string|Exception $message
     * @param array $context
     * @return ResursBankCheckout
     * @throws Exception
     */
    public function log($message, array $context = [])
    {
        $this->getLogHandler()->log($message, $context);

        return $this;
    }

    /**
     * Retrieve log handler.
     *
     * @return \ResursBankCheckout\Models\Log
     */
    private function getLogHandler()
    {
        if ($this->logHandler === null) {
            $this->logHandler = new \ResursBankCheckout\Models\Log();
        }

        return $this->logHandler;
    }

    /**
     * Retrieve simple module link. On certain installations the pretty URLs which can be created and used by PrestaShop
     * simply will not work. We are therefore better of using this approach so routing always works regardless of the
     * circumstances.
     *
     * @param string $controller
     * @param array $params
     * @return string
     */
    public function getSimpleLink($controller, array $params = [], $niceFormat = false)
    {
        if (!$niceFormat) {
            $result = $this->getBaseUrl() .
                '?fc=module&module=resursbankcheckout&controller=' .
                (string)$controller;
        } else {
            $result = $this->getBaseUrl() .
                '/fc/module/module/resursbankcheckout/controller/' .
                (string)$controller;
        }

        if (count($params)) {
            foreach ($params as $key => $val) {
                if (!$niceFormat) {
                    $result .= '&' . (string)$key . '=' . (string)$val;
                } else {
                    $result .= '/' . (string)$key . '/' . (string)$val;
                }
            }
        }

        return $result;
    }
}
