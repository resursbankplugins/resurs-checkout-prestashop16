<!--
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
-->

<script type="text/javascript">

    var RESURSBANK_ADMIN_CONTROLLER_URL = '{$adminControllerUrl}';

    var RESURSBANK_CURRENT_ENVIRONMENT = '{$currentEnvironment}';

    // Makes sure that the module is really enabled before initiating this.
    {if ($module_enabled)}
    // Observe production mode radio button.
    RESURSBANK.CheckoutAdmin.observeProductionMode();
    {/if}

</script>

<div class="panel">
    <div class="panel-heading">
        {l s='Payment methods' mod='resursbankcheckout'}
    </div>
    <div id="resursbank-checkout-fetch-payment-methods-list">
        {if (is_array($paymentMethods) && count($paymentMethods))}
            <div class="row">
                <div class="col-lg-2" style="font-weight: bold">
                    {l s='Title' mod='resursbankcheckout'}
                </div>
                <div class="col-lg-3" style="font-weight: bold">
                    {l s='Activate/deactive "Part pay" in product display' mod='resursbankcheckout'}
                </div>
                <div class="col-lg-3" style="font-weight: bold">
                    {l s='Duration used for calculating part payment (product display)' mod='resursbankcheckout'}
                </div>
            </div>
            {foreach from=$paymentMethods key=methodIndex item=methodBlock}
                <div class="row resurs_method_row">
                    <div class="col-lg-2">{if (isset($methodBlock['description']))}{$methodBlock['description']}{/if}</div>
                    <div class="col-lg-3">
                        {if ($currentAnnuityMethod === $methodBlock['id'])}
                            <a class="list-action-enable action-enabled"
                               href="{$adminControllerUrl}&method={$methodBlock['id']}&set=disabled">
                                <i class="icon-check"></i>
                            </a>
                        {elseif (isset($paymentMethodsAnnuity[$methodBlock['id']]))}
                            <a class="list-action-enable action-disabled"
                               href="{$adminControllerUrl}&method={$methodBlock['id']}&set=enabled">
                                <i class="icon-remove"></i>
                            </a>
                        {/if}
                    </div>
                    <div class="col-lg-3">
                        {if (isset($paymentMethodsAnnuity[$methodBlock['id']]) && $currentAnnuityMethod === $methodBlock['id'])}
                            <select name="RESURSBANK_PARTPAYMENT_DURATION" class="fixed-width-xl"
                                    id="RESURSBANK_PARTPAYMENT_DURATION"
                                    onchange="RESURSBANK.CheckoutAdmin.updateAnnuityDuration(this)">
                                {foreach from=$paymentMethodsAnnuity[$methodBlock['id']] item=factorArray}
                                    <option value="{$factorArray['duration']}"
                                            {if (intval($currentAnnuityDuration) === intval($factorArray['duration']))}selected="selected"{/if}>
                                        {htmlentities($factorArray['paymentPlanName'])}
                                    </option>
                                {/foreach}
                            </select>
                        {/if}
                    </div>
                </div>
            {/foreach}
            <br>
        {/if}
    </div>
    <div id="resursbank-checkout-fetch-payment-methods-status"
         style="display:none;text-align: center;align-content: center;color:#990000;"></div>
    <div class="row">
        <div class="col-lg-3">
            <button id="fetchPaymentsButton"
                    onclick="RESURSBANK.CheckoutAdmin.fetchPaymentMethods()">{l s='Fetch available payment methods' mod='resursbankcheckout'}</button>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-heading">
        {l s='Registered callbacks' mod='resursbankcheckout'}
    </div>
    <div id="resursCallbackListStatus"
         style="display:none;text-align: center;align-content: center;color:#990000;margin-bottom: 10px;"></div>
    <div id="resursbank-callback-list">
        {if is_array($callbacks) && count($callbacks)}
            {foreach from=$callbacks key=callback item=url}
                <div class="row">
                    <div class="col-lg-3">
                        {$callback}
                    </div>
                    <div style="overflow-wrap: break-word;" id="CB_URL_{$callback}">
                        {$url}
                    </div>
                </div>
            {/foreach}
        {/if}
    </div>

    <br>
    <div class="row">
        <div class="col-lg-3">
            <button id="registerCbButton"
                    onclick="RESURSBANK.CheckoutAdmin.registerCallbacks()">{l s='Register callback URLs' mod='resursbankcheckout'}</button>
        </div>
    </div>
</div>

v{$version}{if !empty($ecom)} + EComPHP {$ecom}{/if}
