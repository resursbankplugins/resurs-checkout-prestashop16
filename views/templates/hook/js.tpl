<!--
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
-->

<div id="resursbank-checkout-container" style="display:none;">
    {if count($errorMessages)}
        {foreach $errorMessages as $msg}
        <div class="alert alert-danger">{$msg}</div>
        {/foreach}
    {/if}
    {$iframe}
</div>

<script type="text/javascript">
    (function () {
        Classy.setWarningLevel('warn');

        // Correct iframe placement.
        $('#opc_payment_methods-content').replaceWith(
            $('#resursbank-checkout-container')
        );

        // Display iframe.
        $('#resursbank-checkout-container').show();

        // Initialize checkout.
        RESURSBANK.Checkout.init({
            baseUrl: '{$baseUrl}',
            failureUrl: '{$backUrl}',
            iframeUrl: '{$iframeUrl}',
            saveOrderUrl: '{$saveOrderUrl}',
            saveBillingUrl: '{$saveBillingUrl}',
            saveShippingUrl: '{$saveShippingUrl}',
            savePaymentUrl: '{$savePaymentUrl}',
            storeSsn: {if $storeSsn === true} true {else} false {/if}
        });
    }());
</script>
