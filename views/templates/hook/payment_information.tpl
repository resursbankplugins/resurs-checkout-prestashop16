<!--
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
-->

<div id="resursBankBlueBoxPanel" class="panel" style="display: none;">
    <div class="panel-heading">{l s='Resurs Bank payment information' mod='resursbankcheckout'}</div>
    <div class="panel resurs_orderinfo_container" style="padding:10px;">
        <img style="padding:5px;" src="{$logo}"><br>
        <div class="resurs_orderinfo_text_label">{l s='Reference' mod='resursbankcheckout'}</div>
        <div class="resurs_orderinfo_text_value">{$payment->id}</div>

        <div class="resurs_orderinfo_text_label">{l s='Payment method' mod='resursbankcheckout'}</div>
        <div class="resurs_orderinfo_text_value">{$payment->paymentMethodId} ({$payment->paymentMethodName})</div>

        <div class="resurs_orderinfo_text_label">{l s='Status' mod='resursbankcheckout'}</div>
        <div class="resurs_orderinfo_text_value">{(implode(' | ', $payment->status))}</div>

        <div class="resurs_orderinfo_text_label">{l s='Customer information' mod='resursbankcheckout'}</div>
        <div class="resurs_orderinfo_text_value">
            {$payment->customer->address->fullName}<br>
            {if isset($payment->customer->address->addressRow1)}{$payment->customer->address->addressRow1}{/if}
            {if isset($payment->customer->address->addressRow2)}{$payment->customer->address->addressRow2}{/if}
            {$payment->customer->address->postalCode} {$payment->customer->address->postalArea}
            <br>
            {$payment->customer->address->country}<br>
        </div>
    </div>
</div>
