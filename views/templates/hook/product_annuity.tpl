<script>
    // Must move this.
    function getCostOfPurchaseWindow(readMoreUrl) {
        window.open(
            readMoreUrl,
            'resursCostOfPurchasePopup',
            'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,copyhistory=no,resizable=yes,width=650px,height=740px'
        );
    }
</script>

<div>
    {l s='Pay from' mod='resursbankcheckout'} {$annuityPrice} {$currency}/{l s='month' mod='resursbankcheckout'}.
    <span style="cursor:pointer; font-weight: bold;"
          onclick="getCostOfPurchaseWindow('{$readMoreUrl}')">{l s='Read more' mod='resursbankcheckout'}</span>
</div>