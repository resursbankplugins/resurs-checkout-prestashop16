/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Classy.define({
    name: 'AjaxQ',
    
    init: function AjaxQ (me) {
        me.chains = {};
    },

    proto: {
        /**
         * Creates a new chain. A chain is a queue of ajax calls.
         * 
         * @param  {String} name
         * @return {this}
         */
        createChain: function (name) {
            if (!this.chains.hasOwnProperty(name)) {
                this.chains[name] = Classy.create('AjaxQChain', {
                    name: name
                });
            }

            return this;
        },

        /**
         * Removes the specified chain.
         * 
         * @param  {String} name
         * @return {this}
         */
        removeChain: function (name) {
            var chain = this.chains[name];

            if (chain) {
                chain.$nullify();

                delete this.chains[name];
            }

            return this;
        },

        /**
         * Returns the specifies chain.
         * 
         * @param  {String} name
         * @return {AjaxQChain}
         */
        getChain: function (name) {
            return this.chains[name];
        },

        /**
         * Queues an AJAX call.
         *
         * @param {Object} obj
         * @param {String} obj.url - The URL to send the message to.
         * @param {String} obj.chain - The queue to add to the call to.
         * @param {Object} [obj.data] - An object with data to send with the call.
         * @param {Function} [obj.success] - A callback when the AJAX call has been successful.
         * @param {Function} [obj.failure] - A callback when the AJAX call has failed.
         * @param {Function} [obj.complete] - A callback when the AJAX call has completed.
         * @return {this}
         */
        queue: function (obj) {
            var chain;

            if (obj.hasOwnProperty('url') && typeof obj.url === 'string') {
                if (obj.hasOwnProperty('chain') && typeof obj.chain === 'string') {
                    chain = obj.chain;

                    if (!this.chains.hasOwnProperty(chain)) {
                        throw Error('AJAX queue(): Chain [' + chain + '] does not exist.');
                    }

                    delete obj.chain;

                    this.chains[chain].queue(obj);
                }
            }

            return this;
        },

        /**
         * Runs through an entire chain of calls.
         *
         * @param {String} chainName - The chain to run.
         * @return {this}
         */
        run: function (chainName) {
            var chain = this.getChain(chainName);

            if (chain && !chain.$call('isRunning')) {
                chain.$call('run');
            }

            return this;
        }
    }
});
