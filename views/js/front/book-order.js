/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Classy.define({
    name: 'BookOrder',

    init: function BookOrder (me) {
        /**
         * Checks with the server if the order is ready to be booked.
         *
         * @return {me}
         */
        me.push = function () {
            RESURSBANK.Checkout.AjaxQ.queue({
                chain: 'resursbank-checkout',
                url: RESURSBANK.Checkout.$call('getSaveOrderUrl'),

                success: function (data) {
                    if (data.message.error.length) {
                        me.$emit('push-error', data, {
                            eventType: 'checkout:order-status',
                            orderReady: false
                        });
                    }
                    else {
                        me.$emit('pushed',  {
                            eventType: 'checkout:order-status',
                            orderReady: true
                        });
                    }
                },

                failure: function (data) {
                    me.$emit('push-failed', {
                        eventType: 'checkout:order-status',
                        orderReady: false
                    });
                }
            }).run('resursbank-checkout');

            return me;
        };

        /**
         * Destroys the instance.
         */
        me.destroy = function () {
            me.$nullify();
        };
    }
});
