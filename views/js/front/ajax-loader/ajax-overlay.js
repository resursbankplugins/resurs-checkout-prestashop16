/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Classy.define({
    name: 'AjaxOverlay',

    init: function AjaxOverlay (config, me) {
        /**
         * Initializer.
         * 
         * @param {Object} [config]
         * @param {Boolean} [config.shown] - The value of the [background-position] property.
         */
        var init = function (config) {
            me.$apply(config);

            me.overlay = document.createElement('div');
            me.overlay.className = 'resursbank-checkout-ajax-screen-overlay';

            me.el = document.createElement('div');
            me.el.className = 'resursbank-checkout-ajax-screen-overlay-wrapper';

            me.ajaxLoader = Classy.create('AjaxLoader', {
                appendTo: me.el,
                position: 'center',
                size: '120px 120px'
            });

            me.ajaxLoader.el.style.position = 'absolute';
            me.ajaxLoader.el.style.width = '100%';
            me.ajaxLoader.el.style.height = '100%';

            me.el.appendChild(me.overlay);

            if (me.shown) {
                me.show();
            }
        };

        /**
         * Whether or not the overlay is shown.
         * 
         * @type {Boolean}
         */
        me.shown = false;

        /**
         * The wrapper element for the overlay and ajax loader.
         * 
         * @type {Element}
         */
        me.el = null;

        /**
         * An element that is overlaying the screen.
         * 
         * @type {Element}
         */
        me.overlay = null;

        /**
         * Whether the loader is in the process of showing/hiding.
         *
         * @type {Boolean}
         */
        me.inProgress = false;

        /**
         * An ajax loader.
         * 
         * @type {AjaxLoader}
         */
        me.ajaxLoader = null;

        /**
         * Displays the overlay.
         *
         * @return {me}
         */
        me.show = function () {
            if (!me.shown && !me.inProgress) {
                me.inProgress = true;
                me.el.style.opacity = '0.0';

                document.body.appendChild(me.el);

                me.el.appear({
                    from: 0.0,
                    to: 0.7,
                    duration: 0.5,

                    afterFinish: function () {
                        me.inProgress = false;
                        me.shown = true;
                        me.$emit('shown');
                    }
                });
            }

            return me;
        };

        /**
         * Displays the overlay.
         *
         * @return {me}
         */
        me.hide = function () {
            if (me.shown && !me.inProgress) {
                me.inProgress = true;

                me.el.fade({
                    from: 0.7,
                    to: 0.0,
                    duration: 0.5,

                    afterFinish: function () {
                        document.body.removeChild(me.el);

                        me.inProgress = false;
                        me.shown = false;
                        me.el.style.opacity = '0.0';
                        me.$emit('hidden');
                    }
                });
            }

            return me;
        };

        init(config);
    }
});
