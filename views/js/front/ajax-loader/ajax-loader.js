/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Classy.define({
    name: 'AjaxLoader',

    init: function AjaxLoader (config, me) {
        /**
         * Initializer.
         * 
         * @param {Object} [config]
         * @param {String} [config.position] - The value of the [background-position] property.
         * @param {String} [config.size] - The value of the [background-size] property.
         * @param {Element} [config.appendTo] - The element it will append To.
         */
        var init = function (config) {
            me.$apply(config);

            me.el = document.createElement('div');
            me.el.className = 'resursbank-checkout-ajax-loader';

            if (me.position) {
                me.setPosition(me.position);
            }

            if (me.size) {
                me.setSize(me.size);
            }

            if (config.appendTo) {
                config.appendTo.appendChild(me.el);
            }
        };

        /**
         * The ajax loader element.
         * 
         * @type {Element}
         */
        me.el = null;

        /**
         * The value of its [background-position] property.
         * 
         * @type {String}
         */
        me.position = '';

        /**
         * The value of its [background-size] property.
         * 
         * @type {String}
         */
        me.size = '';

        /**
         * Destroys the instance.
         */
        me.destroy = function () {
            init = null;

            me.$nullify();
        };

        init(config);
    },

    proto: {
        /**
         * Appends the loader to the given element.
         *
         * @param {Element} el
         * @returns {this}
         */
        appendTo: function (el) {
            el.appendChild(this.el);

            return this;
        },

        /**
         * Returns the element.
         * 
         * @return {Element}
         */
        getElement: function () {
            return this.el;
        },

        /**
         * Sets the position of the element using the [background-position] property.
         * 
         * @param {String} position - The string is expected to be any valid CSS string for the [background-position] 
         * property.
         */
        setPosition: function (position) {
            this.position = position;
            this.el.style.backgroundPosition = position;

            return this;
        },

        /**
         * Sets the size of the element using the [background-size] property.
         * 
         * @param {String} size - The string is expected to be any valid CSS string for the [background-size] 
         * property.
         */
        setSize: function (size) {
            this.size = size;
            this.el.style.backgroundSize = size;

            return this;
        }
    }
});
