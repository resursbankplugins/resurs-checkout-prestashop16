/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Classy.define({
    name: 'ErrorHandler',

    init: function ErrorHandler (me) {
        /**
         * A list of messages that should be displayed to the user.
         * 
         * @type {Array<String>}
         */
        var messages = [];

        /**
         * Takes a string and adds it to the list of messages. The messages can then be displayed
         * using [me.alert()].
         * 
         * @param {String} msg
         * @return {me}
         */
        me.feedLine = function (msg) {
            if (typeof msg === 'string') {
                messages.push(msg);
            }

            return me;
        };

        /**
         * Takes a array of string and adds it to the list of messages. The messages can then be 
         * displayed using [me.alert()].
         * 
         * @param {Array<String>} msgs
         * @return {me}
         */
        me.feedLines = function (msgs) {
            if (Array.isArray(msgs)) {
                messages = messages.concat(msgs);
            }

            return me;
        };

        /**
         * Alerts all stored messages and then empties the message list, or if a string [msg] is 
         * provided, then it will alert the provided message and nothing more.
         * 
         * @param {String} msg
         * @return {me}
         */
        me.alert = function (msg) {
            if (typeof msg === 'string') {
                window.alert(msg);
            } else {
                window.alert(messages.join('\n\r'));
                messages = [];
            }

            return me;
        };
    }
});
