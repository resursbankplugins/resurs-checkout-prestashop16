/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Classy.define({
    name: 'Message',

    init: function Message (config, me) {
        /**
         * The current animation object.
         *
         * @type {Object}
         */
        var animation = null;

        /**
         * A setTimeout ID for the animations.
         *
         * @type {String}
         */
        var timeout = null;

        /**
         * An on complete callback.
         *
         * @type {Function}
         */
        var complete = null;

        /**
         * Initializer.
         *
         * @param {Object} config
         * @param {String} [config.text] - The text of the message.
         */
        var init = function (config) {
            me.$apply(config);

            me.el = document.createElement('div');
            me.el.textContent = me.text;
            me.el.className = 'resursbank-checkout-message';
            me.el.style.opacity = 0.0;
            me.el.style.display = 'none';
        };

        /**
         * The message element.
         *
         * @type {Element}
         */
        me.el = null;

        /**
         * The message.
         *
         * @type {string}
         */
        me.text = '';

        /**
         * Whether or not the message is displayed.
         *
         * @type {Boolean}
         */
        me.shown = false;

        /**
         *
         * @param {Number} showFor - The amount of time in seconds the message should be displayed before fading out.
         * @param {Function} completeCallback - Callback for when the entire show/hide process has been completed.
         * @returns {*}
         */
        me.run = function (showFor, completeCallback) {
            if (typeof completeCallback === 'function') {
                complete = completeCallback;
            }

            if (!me.shown) {
                me.show(showFor);
            }

            return me;
        };

        /**
         * Displays the message.
         *
         * @param {Number} showFor - The amount of time in seconds the message should be displayed before fading out.
         * @returns {me}
         */
        me.show = function (showFor) {
            var time = typeof showFor === 'number' ? showFor : 0;

            if (!me.shown) {
                animation = new Effect.Appear(me.el, {
                    from: 0.0,
                    to: 1.0,

                    afterFinish: function () {
                        me.shown = true;

                        timeout = setTimeout(me.hide, time);
                    }
                });
            }

            return animation;
        };

        /**
         * Hides the message.
         *
         * @returns {me}
         */
        me.hide = function () {
            if (me.shown) {
                animation = new Effect.Fade(me.el, {
                    from: 1.0,
                    to: 0.0,

                    afterFinish: function () {
                        if (typeof complete === 'function') {
                            complete()
                        }

                        me.stop();
                    }
                });
            }

            return animation;
        };

        /**
         * Stops the process and returns the element to it's default state.
         *
         * @returns {me}
         */
        me.stop = function () {
            if (timeout) {
                clearTimeout(timeout);
                timeout = null;
            }

            if (animation) {
                animation.cancel();
                animation = null;
            }

            me.shown = false;
            me.el.style.opacity = 0.0;
            me.el.style.display = 'none';

            return me;
        };

        /**
         * Destroys the instance.
         */
        me.destroy = function () {
            init = null;

            me.$nullify();
        };

        init(config);
    }
});
