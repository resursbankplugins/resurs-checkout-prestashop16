/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

var Classy = (function Classy (me) {
    /**
     * A collection of constructor functions.
     * 
     * @type {Object}
     */
    var definitions = {};

    /**
     * The warning level for the [Instance.$call] method.
     * 
     * @type {String}
     */
    var warningLevel = 'warn';

    /**
     * A [toString] method to check for data types.
     * 
     * @type {Function}
     */
    var toString = Object.prototype.toString;

    /**
     * Emitter constructor. Allows instances to manage events (adding, emitting, ignoring). These
     * events are used to communicate with the "parent class", i.e. a class which instantiates 
     * another class, so this has nothing to do with inheritance.
     *
     * @constructor
     */
    var Emitter = function Emitter () {
        this.$callbacks = {};
    };

    /**
     * Static function. Creates a list of arguments from the [arguments] array. Due to optimization 
     * purposes, we do not use the Array.prototype.slice.call() method of creating an argument list.
     *
     * NOTE: If [offset] is higher then the length of [params], or lower than 0, it will be 0.
     * 
     * @param  {Array} params - The [arguments] variable.
     * @param  {Number} offset - Where to start taking arguments.
     * @return {Array} The new list of arguments.
     */
    Emitter.createArgs = function (params, offset) {
        var args = [];
        var len = params.length;
        var i = typeof offset === 'number' ? offset : 0;

        if (i >= len || i < 0) {
            i = 0;
        }

        while (i < len) {
            args.push(params[i]);
            i += 1;
        }

        return args;
    };

    Emitter.prototype = {
        constructor: Emitter,

        /**
         * Creates a listener for an event. Can be used by the parent class to listen for events
         * emitted from the child.
         * 
         * @param  {String} event - The name of the event.
         * @param  {Function} callback - The callback.
         * @return {this}
         */
        $on: function (event, callback) {
            if (!this.$callbacks.hasOwnProperty(event)) {
                this.$callbacks[event] = [];
            }

            this.$callbacks[event].push(callback);

            return this;
        },

        /**
         * Emits an event. Can be used by the child to notify the parent. Accepts additional
         * arguments that will be passed on as arguments to the callbacks for the event.
         * 
         * @param  {String} event - The name of the event.
         * @return {this}
         */
        $emit: function (event) {
            var args = Emitter.createArgs(arguments, 1);

            (this.$callbacks[event] || []).forEach(function (callback) {
                callback.apply(null, args);
            });

            return this;
        },

        /**
         * Removes a callback from an event. Both [event] and [callback] are required.
         * 
         * @param  {String} event
         * @param  {Function} callback
         * @return {this}
         */
        $ignore: function (event, callback) {
            var i = 0;
            var cbs = this.$callbacks[event];

            if (toString.call(cbs) === '[object Array]') {
                if (typeof callback === 'function') {
                    while (i < cbs.length) {
                        if (callback === cbs[i]) {
                            cbs.splice(i, 1);
                            break;
                        }

                        i += 1;
                    }
                } else {
                    while (cbs.length !== 0) {
                        cbs.pop();
                    }
                }
            }

            return this;
        }
    };

    /**
     * Instance constructor. Allows instances to use utility methods.
     *
     * @constructor
     */
    var Instance = function Instance () {};
    Instance.prototype = Object.create(Emitter.prototype);
    Instance.prototype.constructor = Instance;

    /**
     * Allows the instance to merge with a configuration object. It's meant to replace the repetitive for-of loops
     * inside of constructors. The instance will only merge with properties already defined on the instance itself.
     * Alternatively, you could also set the second argument, [applyAll], to [true] to get all properties from the
     * [config] object.
     *
     * @param {Object} config - Object to inherit properties from.
     * @param {Boolean} applyAll - If true, the instance will merge with all the properties found on [config].
     * @returns {Instance}
     */
    Instance.prototype.$apply = function (config, applyAll) {
        var i;

        if (toString.call(config) === '[object Object]') {
            for (i in config) {
                if ((applyAll && config.hasOwnProperty(i)) || this.hasOwnProperty(i)) {
                    this[i] = config[i];
                }
            }
        }

        return this;
    };

    /**
     * Allows the instance to safely call methods of other instances, depending on the [warningLevel] variable. If this
     * variable is set to "warn" or "none", this method will either log a warning in the console or not do anything if
     * an error occurs. If set to "error" it will throw if there's an exception.
     *
     * @param {String} name - The name of the method you want to call.
     * @returns {*} The return value of the method you call.
     */
    Instance.prototype.$call = function (name) {
        var retValue;
        var args = Emitter.createArgs(arguments, 1);

        try {
            retValue = this[name].apply(this, args);
        } catch (e) {
            if (warningLevel === 'warn') {
                console.warn("$call(): Error when calling [" + name + "] as a function. \n", e);
            } else if (warningLevel === 'error') {
                throw Error("$call(): Error when calling [" + name + "] as a function. \n", e);
            }
        }

        return retValue;
    };

    /**
     * Sets every property of the instance to null. This is useful when you want to remove the instance, as using the
     * [delete] keyword can be slower in some browsers (like Chrome).
     *
     * @returns {Instance}
     */
    Instance.prototype.$nullify = function () {
        var i;

        for (i in this) {
            if (this.hasOwnProperty(i)) {
                this[i] = null;
            }
        }

        return this;
    };

    /**
     * Let's the instance inherit from other constructors. The instance will be sent in as the specified constructor's
     * [this] and [me] argument. You cannot inherit another constructor's prototype.
     *
     * NOTE: This will not make so that [myInstance instanceof InheritedClass] will work! [myInstance] will not be a
     * child of [InheritedClass].
     *
     * @param {String} name - Name of the constructor to inherit from.
     * @param {Object} [config] - Optional configuration object.
     * @returns {Instance}
     */
    Instance.prototype.$inherit = function (name, config) {
        var def = definitions[name];

        def.orig.init.call(this, config, this);

        return this;
    };

    /**
     * Takes a string and sets the warning level to either "warn", "error" or "none". The warning level says what to do
     * when an error occurs in some function. "warn" will log the error in the console; if set to "none", nothing will
     * happen; and "error" will throw the error like normal.
     *
     * @param {String} level
     * @returns {me}
     */
    me.setWarningLevel = function (level) {
        if (level === 'warn' || level === 'error' || level === 'none') {
            warningLevel = level;
        }

        return me;
    };

    /**
     * Returns the warning level.
     *
     * @returns {String}
     */
    me.getWarningLevel = function () {
        return warningLevel;
    };

    /**
     * Defines a class. Takes an object with a name, constructor and an optional prototype object, and returns a new
     * constructor function. The new constructor makes sure that the instances from your constructor inherits methods
     * from [Instance] and [Emitter].
     *
     * NOTE: If you want to be able to use the [instanceof] operator with you instances, you should use a named
     * function expressions for your constructor: {init: function Hello () {}}. This will also make the returned
     * constructor be a [Hello] constructor instead of an anonymous function.
     *
     * @param {Object} def
     * @param {String} def.name - The name of the definition.
     * @param {Function} def.init - The constructor function.
     * @param {Object} [def.proto] - An object that acts as the instance's prototype property.
     * @returns {Function} A new constructor.
     */
    me.define = function (def) {
        var i;

        // New constructor function.
        var init = function (config) {
            var instance = Object.create(init.prototype);
            var args = [];

            if (typeof config !== 'undefined') {
                args.push(config);
            }

            args.push(instance);

            Emitter.call(instance);
            def.init.apply(instance, args);

            return instance;
        };

        definitions[def.name] = {
            orig: def,
            init: init
        };

        init.prototype = Object.create(Instance.prototype);
        init.prototype.constructor = def.init;

        if (def.proto) {
            for (i in def.proto) {
                if (def.proto.hasOwnProperty(i)) {
                    init.prototype[i] = def.proto[i];
                }
            }
        }

        return init;
    };

    /**
     * Takes a name and an optional argument and creates an instance, passing in the optional argument to its
     * constructor. If you set [name] to [null] or if you set the warning level to "none" it will return an instance of
     * [Instance].
     *
     * @param {String} name - The name of a constructor.
     * @param {*} [config]
     * @returns {*} The new instance. If [name] is [null] then an instance of [Instance] will be returned.
     */
    me.create = function (name, config) {
        var instance;

        if (typeof name === 'string') {
            if (warningLevel === 'warn' || warningLevel === 'none') {
                try {
                    instance = definitions[name].init(config);
                } catch (e) {
                    if (warningLevel === 'warn') {
                        console.warn("Instance of [" + name + "] can't be created. \n", e);
                    }

                    instance = new Instance();
                    Emitter.call(instance);
                }
            } else {
                instance = definitions[name].init(config);
            }
        } else if (name === null) {
            instance = new Instance();
            Emitter.call(instance);
        }

        return instance;
    };

    /**
     * Takes an instance and a name of a constructor, and returns [true] or [false] whether [obj] is an instance of the
     * constructor. It will throw, warn and/or return false, depending on the warning level, if something goes wrong.
     *
     * @param {Object} instance
     * @param {String} name - Name of the constructor.
     * @returns {Boolean}
     */
    me.is = function (instance, name) {
        var is;
        var def = definitions[name];

        if (!def) {
            switch (name) {
                case 'Emitter':
                    def = Emitter;
                    break;

                case 'Instance':
                    def = Instance;
                    break;

                default: def = null;
            }
        }

        try {
            is = instance instanceof def;
        } catch (e) {
            if (warningLevel === 'warn') {
                console.warn('Classy.is(): Could not perform [instanceof] check for [' + name + ']\n', e);
            } else if (warningLevel === 'error') {
                throw Error('Classy.is(): Could not perform [instanceof] check for [' + name + ']\n', e);
            }

            is = false;
        }

        return is;
    };

    return me;
}({}));

RESURSBANK.Classy = Classy;
